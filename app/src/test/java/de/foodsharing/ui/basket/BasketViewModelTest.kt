package de.foodsharing.ui.basket

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.whenever
import de.foodsharing.api.BasketAPI
import de.foodsharing.api.UserResponse
import de.foodsharing.services.AuthService
import de.foodsharing.services.BasketService
import de.foodsharing.test.configureTestSchedulers
import de.foodsharing.test.createRandomBasket
import de.foodsharing.ui.baskets.CurrentUserLocation
import io.reactivex.Observable.just
import io.reactivex.subjects.PublishSubject
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class BasketViewModelTest {

    @Mock
    lateinit var basketService: BasketService
    @Mock
    lateinit var authService: AuthService
    @Mock
    lateinit var currentUserLocation: CurrentUserLocation

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        configureTestSchedulers()
    }

    @Test
    fun `fetch basket`() {
        val authSubject = PublishSubject.create<UserResponse>()
        whenever(authService.currentUser()) doReturn authSubject

        val basket = createRandomBasket()
        whenever(basketService.get(anyInt())) doReturn just(BasketAPI.BasketResponse().apply {
            this.basket = basket
        })

        val viewModel = BasketViewModel(basketService, authService, currentUserLocation)
        viewModel.basketId = basket.id

        val b = viewModel.basket.value!!
        Assert.assertEquals(b, basket)
    }
}
