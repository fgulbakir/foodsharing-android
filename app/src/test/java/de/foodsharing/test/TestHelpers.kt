package de.foodsharing.test

import de.foodsharing.api.Profile
import de.foodsharing.api.UserResponse
import de.foodsharing.model.Basket
import de.foodsharing.model.ConversationListEntry
import de.foodsharing.model.FairSharePoint
import de.foodsharing.model.User
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.Date
import kotlin.random.Random

fun configureTestSchedulers() {
    // See https://medium.com/@dbottillo/how-to-unit-test-your-rxjava-code-in-kotlin-d239364687c9
    RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
    RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
    RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
    RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
}

fun createRandomUserResponse(id: Int = Random.nextInt()): UserResponse {
    return UserResponse(id, "user$id")
}

fun createRandomUser(id: Int = Random.nextInt()): User {
    return User(id, "user$id@foo.com", "user$id", null, null)
}

fun createRandomProfile(id: Int = Random.nextInt(), hasAddress: Boolean = true): Profile {
    return Profile(
        id,
        "user$id",
        "user$id",
        if (hasAddress) "Hauptstraße 1" else null,
        if (hasAddress) "Berlin" else null,
        if (hasAddress) "12345" else null,
        if (hasAddress) Random.nextDouble(-90.0, 90.0).toString() else null,
        if (hasAddress) Random.nextDouble(-180.0, 180.0).toString() else null,
        "user$id@foo.com",
        null,
        null
    )
}

fun createRandomBasket(creator: User = createRandomUser()) = Basket(
    Random.nextInt(),
    "description",
    null,
    Date(),
    Date(),
    arrayOf(1, 2),
    Date(),
    Random.nextDouble(-90.0, 90.0),
    Random.nextDouble(-180.0, 180.0),
    0,
    emptyList(),
    creator,
    "+49 030 123456",
    "+49 1576 123456"
)

fun createRandomFairSharePoint() = FairSharePoint(
    Random.nextInt(),
    Random.nextInt(),
    "name",
    "description",
    "address",
    "city",
    "12345",
    Date(),
    null,
    Random.nextDouble(-90.0, 90.0),
    Random.nextDouble(-180.0, 180.0)
)

fun createRandomConversations(nums: Int, currentUser: User = createRandomUser()): List<ConversationListEntry> {
    val conversations = mutableListOf<ConversationListEntry>()
    for (i in 1..nums) {
        val conversationId = Random.nextInt()
        val last = Date()
        val lastTSString = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(last)

        val users = mutableListOf<User>(currentUser, createRandomUser())
        if (Random.nextBoolean()) {
            users.add(createRandomUser())
        }

        conversations.add(
            ConversationListEntry(
                conversationId,
                lastTSString,
                users.random().id,
                "last message",
                last,
                null,
                Random.nextInt(2),
                users
            )
        )
    }
    return conversations
}
