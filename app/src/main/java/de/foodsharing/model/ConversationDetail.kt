package de.foodsharing.model

/**
 * A conversation in the chat between multiple users.
 */
data class ConversationDetail(
    val members: List<User>,
    val messages: List<Message>,
    val name: String?
)