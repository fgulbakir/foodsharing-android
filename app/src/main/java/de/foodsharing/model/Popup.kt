package de.foodsharing.model

import com.google.gson.annotations.SerializedName

data class Popup(
    val title: String?,
    val body: String?,
    val conditions: List<PopupCondition>,
    @SerializedName("positiveAction")
    val positiveAction: PopupAction?,
    @SerializedName("negativeAction")
    val negativeAction: PopupAction?
)

data class PopupCondition(
    val type: PopupConditionType?,
    val value: String
)

data class PopupAction(
    val title: String,
    val url: String?
)

enum class PopupConditionType {
    VERSION_CODE,
    LOCALE,
    BUILD_TYPE,
    FLAVOR
}
