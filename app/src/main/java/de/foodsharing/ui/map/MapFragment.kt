package de.foodsharing.ui.map

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.model.Coordinate
import de.foodsharing.model.FairSharePoint
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.basket.BasketActivity
import de.foodsharing.ui.fsp.FairSharePointActivity
import de.foodsharing.utils.DEFAULT_MAP_ZOOM
import de.foodsharing.utils.LOG_TAG
import de.foodsharing.utils.SHARED_PREFERENCE_MAP_CENTER_LAT
import de.foodsharing.utils.SHARED_PREFERENCE_MAP_CENTER_LON
import de.foodsharing.utils.SHARED_PREFERENCE_MAP_ZOOM
import de.foodsharing.utils.Utils.getBasketMarkerIcon
import de.foodsharing.utils.Utils.getFspMarkerIcon
import kotlinx.android.synthetic.main.fragment_map.view.map_layers_button
import org.osmdroid.bonuspack.clustering.RadiusMarkerClusterer
import org.osmdroid.util.BoundingBox
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.Marker
import javax.inject.Inject

class MapFragment : BaseMapFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: MapViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MapViewModel::class.java)
    }

    @Inject
    lateinit var preferences: SharedPreferences

    private lateinit var markerOverlay: RadiusMarkerClusterer
    private var basketMarkers: List<Marker>? = null
    private var fspMarkers: List<Marker>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        if (!super.canRestoreMapCamera(savedInstanceState)) {
            // try to restore old camera view or zoom out to germany
            if (!restoreMapCamera()) {
                val initialBoundingBox = BoundingBox.fromGeoPoints(
                        listOf(GeoPoint(46.0, 4.0), GeoPoint(55.0, 17.0)))
                mapView.addOnFirstLayoutListener { _, _, _, _, _ ->
                    mapView.zoomToBoundingBox(initialBoundingBox, false)
                }
            }
        }

        context?.let {
            markerOverlay = FsRadiusMarkerClusterer(it).apply {
                onClusterClickListener = this@MapFragment::onClusterClick
            }
            markerOverlay.setRadius((it.resources.displayMetrics.density * 46).toInt())
            markerOverlay.setMaxClusteringZoomLevel(Integer.MAX_VALUE)

            mapView.overlays.add(markerOverlay)
        }

        setupLayersButton(view)

        bindViewModel()

        return view
    }

    private fun setupLayersButton(view: View?) {
        val layersButton = view?.map_layers_button ?: return

        val contextThemeWrapper = ContextThemeWrapper(context, R.style.PopupMenuOverlapAnchor)
        val popup = PopupMenu(contextThemeWrapper, layersButton, Gravity.TOP or Gravity.END)
        popup.menuInflater.inflate(R.menu.layer_menu, popup.menu)

        popup.menu.findItem(R.id.basket_layer_item).isChecked = viewModel.isShowingBaskets.value == true
        popup.menu.findItem(R.id.fsp_layer_item).isChecked = viewModel.isShowingFSPs.value == true

        popup.setOnMenuItemClickListener { item ->
            item.isChecked = !item.isChecked
            when (item.itemId) {
                R.id.basket_layer_item -> viewModel.isShowingBaskets.value = item.isChecked
                R.id.fsp_layer_item -> viewModel.isShowingFSPs.value = item.isChecked
            }
            updateMarkers()
            false
        }

        layersButton.setOnClickListener {
            context?.let {
                popup.show()
            }
        }
    }

    private fun bindViewModel() {
        viewModel.showError.observe(this, EventObserver {
            showMessage(getString(it))
        })

        viewModel.markers.observe(this, Observer {
            setMarkers(
                it?.first ?: emptyList<FairSharePoint>(),
                it?.second ?: emptyList<Basket>()
            )
        })

        viewModel.isShowingBaskets.observe(this, Observer {
            updateMarkers()
        })
        viewModel.isShowingFSPs.observe(this, Observer {
            updateMarkers()
        })
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_map
    }

    private fun restoreMapCamera(): Boolean {
        if (!(preferences.contains(SHARED_PREFERENCE_MAP_CENTER_LAT) &&
                        preferences.contains(SHARED_PREFERENCE_MAP_CENTER_LON) &&
                        preferences.contains(SHARED_PREFERENCE_MAP_ZOOM))) {
            return false
        }
        val centerLat = preferences.getFloat(SHARED_PREFERENCE_MAP_CENTER_LAT, 0.0f).toDouble()
        val centerLon = preferences.getFloat(SHARED_PREFERENCE_MAP_CENTER_LON, 0.0f).toDouble()
        val zoom = preferences.getFloat(SHARED_PREFERENCE_MAP_ZOOM, DEFAULT_MAP_ZOOM.toFloat()).toDouble()
        updateLocation(Coordinate(centerLat, centerLon), zoom, false)
        return true
    }

    private fun storeMapCamera() {
        val centerLat = mapView.boundingBox.centerLatitude
        val centerLon = mapView.boundingBox.centerLongitude
        val zoom = mapView.zoomLevelDouble
        preferences.edit()
                .putFloat(SHARED_PREFERENCE_MAP_CENTER_LAT, centerLat.toFloat())
                .putFloat(SHARED_PREFERENCE_MAP_CENTER_LON, centerLon.toFloat())
                .putFloat(SHARED_PREFERENCE_MAP_ZOOM, zoom.toFloat())
                .apply()
    }

    private fun setMarkers(fairSharePoints: List<FairSharePoint>, baskets: List<Basket>) {
        // add fair-share-point markers
        val fspIcon = getFspMarkerIcon(context!!)
        fspMarkers = fairSharePoints.map { fsp ->
            val marker = Marker(mapView)
            marker.id = fsp.id.toString()
            marker.position = GeoPoint(fsp.lat, fsp.lon)
            marker.icon = fspIcon
            marker.relatedObject = fsp
            marker.setOnMarkerClickListener { m, _ ->
                val fairsp = m.relatedObject as FairSharePoint
                Log.v(LOG_TAG, "$fairsp")

                // show fsp details in an activity
                val intent = Intent(context, FairSharePointActivity::class.java)
                intent.putExtra(FairSharePointActivity.EXTRA_FSP_ID, fairsp.id)
                startActivity(intent)
                activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                true
            }
            marker
        }

        val basketIcon = getBasketMarkerIcon(context!!)
        basketMarkers = baskets.map { b ->
            val marker = Marker(mapView)
            marker.id = b.id.toString()
            marker.position = GeoPoint(b.lat, b.lon)
            marker.icon = basketIcon
            marker.relatedObject = b
            marker.setOnMarkerClickListener { m, _ ->
                val basket = m.relatedObject as Basket
                Log.v(LOG_TAG, "$basket")

                // show basket details in an activity
                val intent = Intent(context, BasketActivity::class.java)
                intent.putExtra(BasketActivity.EXTRA_BASKET_ID, basket.id)
                startActivity(intent)
                activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                true
            }
            marker
        }

        updateMarkers()
    }

    private fun onClusterClick(markers: List<Marker>) {
        val ctx = context ?: return
        val builder = AlertDialog.Builder(ctx)

        val arrayAdapter = ArrayAdapter<String>(ctx, android.R.layout.select_dialog_item)
        markers.map {
            when (val relatedObj = it.relatedObject) {
                is Basket -> "${getString(R.string.basket_label)} #${relatedObj.id}"
                is FairSharePoint -> "${getString(R.string.fairsharepoint_label)} #${relatedObj.id}"
                else -> getString(R.string.unknown)
            }
        }.forEach { arrayAdapter.add(it) }

        builder.setNegativeButton(android.R.string.cancel, null)

        builder.setAdapter(arrayAdapter) { _, index ->
            val intent = when (val relatedObj = markers[index].relatedObject) {
                is Basket -> Intent(context, BasketActivity::class.java)
                    .apply { putExtra(BasketActivity.EXTRA_BASKET_ID, relatedObj.id) }
                is FairSharePoint -> Intent(context, FairSharePointActivity::class.java)
                    .apply { putExtra(FairSharePointActivity.EXTRA_FSP_ID, relatedObj.id) }
                else -> return@setAdapter
            }
            startActivity(intent)
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }
        builder.show()
    }

    private fun updateMarkers() {
        markerOverlay.items.clear()

        if (viewModel.isShowingBaskets.value == true) {
            basketMarkers?.forEach {
                markerOverlay.add(it)
            }
        }
        if (viewModel.isShowingFSPs.value == true) {
            fspMarkers?.forEach {
                markerOverlay.add(it)
            }
        }

        markerOverlay.invalidate()
        mapView.invalidate()
    }

    override fun onDestroyView() {
        storeMapCamera()
        super.onDestroyView()
    }
}
