package de.foodsharing.ui.map

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.api.MapAPI
import de.foodsharing.model.Basket
import de.foodsharing.model.FairSharePoint
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import javax.inject.Inject

class MapViewModel @Inject constructor(
    mapAPI: MapAPI
) : BaseViewModel() {
    val showError = MutableLiveData<Event<Int>>()
    val markers = MutableLiveData<Pair<List<FairSharePoint>, List<Basket>>>()
    val isShowingFSPs = MutableLiveData<Boolean>().apply { value = true }
    val isShowingBaskets = MutableLiveData<Boolean>().apply { value = true }

    init {
        request(mapAPI.coordinates(), { response ->
            markers.value = Pair(response.fairteiler, response.baskets)
        }, {
            showError.value = Event(R.string.error_unknown)
        })
    }
}