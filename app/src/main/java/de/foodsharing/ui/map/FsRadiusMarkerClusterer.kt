package de.foodsharing.ui.map

import android.content.Context
import de.foodsharing.utils.CLUSTER_CLICK_ZOOM
import org.osmdroid.bonuspack.clustering.RadiusMarkerClusterer
import org.osmdroid.bonuspack.clustering.StaticCluster
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker

class FsRadiusMarkerClusterer(context: Context) : RadiusMarkerClusterer(context) {
    var onClusterClickListener: ((List<Marker>) -> Unit)? = null

    override fun buildClusterMarker(cluster: StaticCluster, mapView: MapView): Marker {
        val marker = super.buildClusterMarker(cluster, mapView)
        marker.setOnMarkerClickListener { _, view ->
            if (view.zoomLevelDouble >= CLUSTER_CLICK_ZOOM) {
                onClusterClickListener?.invoke((0 until cluster.size).map { cluster.getItem(it) })
                onClusterClickListener != null
            } else {
                false
            }
        }
        return marker
    }
}