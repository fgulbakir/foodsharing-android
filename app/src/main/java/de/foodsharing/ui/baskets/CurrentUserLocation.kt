package de.foodsharing.ui.baskets

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import de.foodsharing.model.Coordinate
import de.foodsharing.utils.LocationFinder
import de.foodsharing.utils.testing.OpenForTesting

@OpenForTesting
class CurrentUserLocation(val context: Context) {
    var currentUserCoordinates = MutableLiveData<Coordinate>()

    private var locationCallbackCancelable: (() -> Unit)? = null

    fun findLocation(): Coordinate {

        if (ContextCompat.checkSelfPermission(context, BasketsFragment.ACCESS_COARSE_LOCATION_PERMISSION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, BasketsFragment.ACCESS_FINE_LOCATION_PERMISSION) == PackageManager.PERMISSION_GRANTED) {

            locationCallbackCancelable?.let { it() }

            val callback = LocationFinder.instance.requestLocation {
                currentUserCoordinates.postValue(Coordinate(it.latitude, it.longitude))
            }
            locationCallbackCancelable = {
                callback()
            }
        }
        return Coordinate(0.0, 0.0)
    }
    init {
        findLocation()
    }
}