package de.foodsharing.ui.baskets

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.basket.BasketActivity
import de.foodsharing.ui.newbasket.NewBasketActivity
import de.foodsharing.utils.LINK_BASE_URL
import kotlinx.android.synthetic.main.fragment_baskets.*
import kotlinx.android.synthetic.main.fragment_baskets.view.*
import javax.inject.Inject

class BasketsFragment : BaseFragment(), BasketActionListener, Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val basketsViewModel: BasketsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(BasketsViewModel::class.java)
    }

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var adapter: BasketListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_baskets, container, false)
        layoutManager = LinearLayoutManager(activity)
        view.recycler_view.layoutManager = layoutManager

        adapter = BasketListAdapter(this, BasketDateFormatter(context!!), context!!)
        view.recycler_view.adapter = adapter

        view.pull_refresh.setOnRefreshListener {
            basketsViewModel.reload()
        }

        val divider = DividerItemDecoration(context, layoutManager.orientation)
        context
                ?.let { ContextCompat.getDrawable(it, R.drawable.abc_list_divider_material) }
                ?.let { divider.setDrawable(it) }
        view.recycler_view.addItemDecoration(divider)

        view.add_basket_button.setOnClickListener {
            val intent = Intent(context, NewBasketActivity::class.java)
            startActivity(intent)
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }

        view.grant_permission.setOnClickListener {
            activity?.let { activity ->
                ActivityCompat.requestPermissions(activity,
                        arrayOf(ACCESS_FINE_LOCATION_PERMISSION, ACCESS_COARSE_LOCATION_PERMISSION),
                        PERMISSION_CODE)
            }
        }

        askPermissionIfNotGranted(view)

        view.no_baskets_label.movementMethod = LinkMovementMethod.getInstance()

        bindViewModel()
        return view
    }

    private fun bindViewModel() {
        basketsViewModel.isLoading.observe(this, Observer {
            if (it) {
                no_baskets_label.visibility = View.GONE

                if (adapter.itemCount > 0) {
                    pull_refresh.isRefreshing = true
                } else {
                    progress_bar.visibility = View.VISIBLE
                }
            } else {
                pull_refresh.isRefreshing = false
                progress_bar.visibility = View.GONE
            }
        })

        basketsViewModel.currentLocationCoordinates.observe(this, Observer {
            it.let {
                val prevCoordinate = basketsViewModel.previousCoordinate
                if (it.lat != 0.0 && it.lon != 0.0 && prevCoordinate.lat != it.lat && prevCoordinate.lon != it.lon) {
                    basketsViewModel.previousCoordinate = it
                    this.basketsViewModel.reload()
                }
            }
        })

        basketsViewModel.showError.observe(this, EventObserver {
            showMessage(getString(it))
        })

        basketsViewModel.baskets.observe(this, Observer {
            val isEmpty = it.first.isEmpty() && it.second.isEmpty()

            if (isEmpty) {
                recycler_view.visibility = View.GONE
                no_baskets_label.visibility = View.VISIBLE
            } else {
                recycler_view.visibility = View.VISIBLE
                no_baskets_label.visibility = View.GONE

                adapter.setBaskets(it.first, it.second)
            }
        })

        basketsViewModel.profile.observe(this, Observer {
            if (it.getCoordinates() != null) {
                no_baskets_label.setText(R.string.baskets_none)
            } else {
                no_baskets_label.text =
                    HtmlCompat.fromHtml(getString(R.string.baskets_none_no_address, LINK_BASE_URL),
                        HtmlCompat.FROM_HTML_MODE_LEGACY)
            }
        })
    }

    override fun onViewBasket(basket: Basket) {
        // show basket details in an activity
        val intent = Intent(context, BasketActivity::class.java)
        intent.putExtra(BasketActivity.EXTRA_BASKET_ID, basket.id)
        startActivity(intent)
        activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }

    fun resetUsersLocation() {
        this.basketsViewModel.getCurrentUsersLocation()
    }

    private fun askPermissionIfNotGranted(view: View) {
        activity?.let {

            if (ContextCompat.checkSelfPermission(it, ACCESS_FINE_LOCATION_PERMISSION) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(it, ACCESS_COARSE_LOCATION_PERMISSION) != PackageManager.PERMISSION_GRANTED) {
                view.grant_permission.show()
            }
        }
    }

    fun hideLocationButton() {
        view?.let {
            it.grant_permission.hide()
        }
    }

    companion object {
        const val PERMISSION_CODE = 0
        const val ACCESS_FINE_LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION
        const val ACCESS_COARSE_LOCATION_PERMISSION = Manifest.permission.ACCESS_COARSE_LOCATION
    }
}
