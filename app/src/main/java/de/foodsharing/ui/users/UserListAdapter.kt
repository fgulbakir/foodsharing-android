package de.foodsharing.ui.users

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import de.foodsharing.R
import de.foodsharing.model.User
import de.foodsharing.utils.Utils
import de.foodsharing.utils.getDisplayName
import de.foodsharing.utils.inflate
import kotlinx.android.synthetic.main.item_user.view.item_name
import kotlinx.android.synthetic.main.item_user.view.item_picture

class UserListAdapter(
    private val onClickListener: (User) -> Unit,
    val context: Context
) : RecyclerView.Adapter<UserListAdapter.UserHolder>() {
    private var users = emptyList<User>()

    override fun getItemCount() = users.size

    override fun onBindViewHolder(holder: UserHolder, position: Int) {
        if (position in 0 until itemCount) {
            holder.bind(users[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): UserHolder {
        return UserHolder(parent.inflate(R.layout.item_user, false), onClickListener, context)
    }

    fun setUsers(users: List<User>) {
        this.users = users
        notifyDataSetChanged()
    }

    class UserHolder(
        val view: View,
        private val onClickListener: (User) -> Unit,
        val context: Context
    ) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private var user: User? = null

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            user?.let {
                onClickListener(it)
            }
        }

        fun bind(user: User) {
            this.user = user
            view.item_name.text = user.getDisplayName(context)
            Picasso.get()
                .load(Utils.getUserPhotoURL(user, Utils.PhotoType.Q_130))
                .fit()
                .centerCrop()
                .error(R.drawable.default_user_picture)
                .into(view.item_picture)
        }
    }
}