package de.foodsharing.ui.users

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.model.User
import de.foodsharing.services.ConversationsService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import java.io.IOException
import javax.inject.Inject

class UserListViewModel @Inject constructor(
    val conversationsService: ConversationsService
) : BaseViewModel() {
    var conversationId: Int? = null
        set(value) {
            if (field != value) {
                field = value
                fetch()
            }
        }

    val users = MutableLiveData<List<User>>()
    val isLoading = MutableLiveData<Boolean>().apply { value = true }
    val showError = MutableLiveData<Event<Int>>()

    private fun fetch() {
        conversationId?.let {
            isLoading.value = true
            request(conversationsService.getUsers(it), {
                isLoading.postValue(false)
                users.postValue(it)
            }, {
                isLoading.postValue(false)
                when (it) {
                    is IOException -> {
                        // Network Error
                        showError.postValue(Event(R.string.error_no_connection))
                    }
                    else -> {
                        // HttpException or unknown exception
                        showError.postValue(Event(R.string.error_unknown))
                    }
                }
            })
        }
    }
}