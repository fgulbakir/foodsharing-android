package de.foodsharing.ui.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.HIDE_NOT_ALWAYS
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.main.MainActivity
import de.foodsharing.utils.INTENT_EXTRA_GO_BACK
import de.foodsharing.utils.INTENT_EXTRA_REQUEST_CODE
import de.foodsharing.utils.LINK_BASE_URL
import de.foodsharing.utils.Utils
import kotlinx.android.synthetic.main.activity_login.progress_bar
import kotlinx.android.synthetic.main.activity_login_form.forgot_password_link
import kotlinx.android.synthetic.main.activity_login_form.login_button
import kotlinx.android.synthetic.main.activity_login_form.password_field
import kotlinx.android.synthetic.main.activity_login_form.register_button
import kotlinx.android.synthetic.main.activity_login_form.user_field
import kotlinx.android.synthetic.main.activity_login_form.version_text_view
import javax.inject.Inject

class LoginActivity : BaseActivity(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val loginViewModel: LoginViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)
        rootLayoutID = android.R.id.content

        bindViewModel()

        login_button.setOnClickListener {
            // Hide soft keyboard
            this.currentFocus?.let {
                val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(it.windowToken, HIDE_NOT_ALWAYS)
            }
            // Trigger login
            loginViewModel.login(user_field.text.toString(), password_field.text.toString())
        }

        register_button.setOnClickListener {
            Utils.openUrl(this, "$LINK_BASE_URL/?page=content&sub=joininfo")
        }

        password_field.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                login_button.performClick()
                return@setOnEditorActionListener true
            }
            false
        }

        setupVersionText()
        setupForgotPasswordLink()
    }

    private fun bindViewModel() {
        loginViewModel.isLoading.observe(this, Observer<Boolean> {
            this.showProgress(it)
        })

        loginViewModel.showError.observe(this, EventObserver {
            showMessage(getString(it))
        })

        loginViewModel.loginFinished.observe(this, EventObserver {
            if (intent.getBooleanExtra(INTENT_EXTRA_GO_BACK, false)) {
                setResult(RESULT_OK)
                finishActivity(intent.getIntExtra(INTENT_EXTRA_REQUEST_CODE, 0))
            } else startActivity(Intent(this, MainActivity::class.java))
            finish()
        })

        loginViewModel.popup.observe(this, EventObserver {
            Utils.handlePopup(this, it)
        })
    }

    private fun setupVersionText() {
        val version = getString(R.string.version, BuildConfig.VERSION_NAME)
        val reportIssue = getString(R.string.report_login_issue)
        val versionText = SpannableString("$version - $reportIssue")
        versionText.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                Utils.openSupportEmail(this@LoginActivity, R.string.support_email_subject_login_suffix)
            }
        }, versionText.length - reportIssue.length, versionText.length, 0)
        version_text_view.movementMethod = LinkMovementMethod.getInstance()

        version_text_view.text = versionText
    }

    private fun setupForgotPasswordLink() {
        val text = SpannableString(forgot_password_link.text)
        text.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                Utils.openUrl(this@LoginActivity, "$LINK_BASE_URL/?page=login&sub=passwordReset")
            }
        }, 0, text.length, 0)
        forgot_password_link.movementMethod = LinkMovementMethod.getInstance()
        forgot_password_link.text = text
    }

    private fun showProgress(show: Boolean) {
        progress_bar.visibility = if (show) {
            View.VISIBLE
        } else {
            View.INVISIBLE
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }
}
