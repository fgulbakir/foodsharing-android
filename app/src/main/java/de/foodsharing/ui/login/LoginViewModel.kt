package de.foodsharing.ui.login

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.api.LoginRequest
import de.foodsharing.api.PopupAPI
import de.foodsharing.model.Popup
import de.foodsharing.services.AuthService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import de.foodsharing.utils.POPUP_CONFIG_ENDPOINT
import retrofit2.HttpException
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    val authService: AuthService,
    val popupAPI: PopupAPI
) : BaseViewModel() {
    val isLoading = MutableLiveData<Boolean>()
    val showError = MutableLiveData<Event<Int>>()
    val loginFinished = MutableLiveData<Event<Int>>()

    val popup = MutableLiveData<Event<List<Popup>>>()

    init {
        request(popupAPI.current(POPUP_CONFIG_ENDPOINT), {
            popup.postValue(Event(it))
        }, {
            // Ignore error, popup will be shown on next launch
        })
    }

    fun login(email: String, password: String) {
        isLoading.value = true

        request(authService.login(LoginRequest(email, password, true)), { user ->
            isLoading.value = false
            loginFinished.value = Event(user.id)
        }, { error ->
            isLoading.value = false

            val stringRes = if (error is HttpException && error.code() == 401) {
                R.string.invalid_password
            } else {
                R.string.error_unknown
            }
            showError.value = Event(stringRes)
        })
    }
}