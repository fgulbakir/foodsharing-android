package de.foodsharing.ui.editbasket

import de.foodsharing.model.Basket
import de.foodsharing.model.Coordinate
import de.foodsharing.ui.base.BaseContract
import java.io.File

class EditBasketContract {

    interface View : BaseContract.View {

        /**
         * Called after the edited basket has been saved.
         */
        fun display(basket: Basket)

        /**
         * Called if saving failed.
         */
        fun showError(message: String)
    }

    interface Presenter : BaseContract.Presenter<View> {

        fun update(
            basketId: Int,
            description: String,
            coordinate: Coordinate,
            picture: File?
        )
    }
}