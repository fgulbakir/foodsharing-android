package de.foodsharing.ui.newbasket

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import com.google.android.material.snackbar.Snackbar
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import de.foodsharing.R
import de.foodsharing.api.Profile
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.model.Coordinate
import de.foodsharing.ui.base.AuthRequiredBaseActivity
import de.foodsharing.ui.basket.BasketActivity
import de.foodsharing.ui.basket.PickLocationActivity
import de.foodsharing.utils.DETAIL_MAP_ZOOM
import de.foodsharing.utils.OsmdroidUtils
import de.foodsharing.ui.picture.PictureFragment
import de.foodsharing.utils.Utils
import de.foodsharing.utils.Utils.getBasketMarkerIconBitmap
import kotlinx.android.synthetic.main.activity_new_basket.basket_checkbox_message
import kotlinx.android.synthetic.main.activity_new_basket.basket_checkbox_phone
import kotlinx.android.synthetic.main.activity_new_basket.basket_description_input
import kotlinx.android.synthetic.main.activity_new_basket.basket_location_view
import kotlinx.android.synthetic.main.activity_new_basket.basket_mobile_input
import kotlinx.android.synthetic.main.activity_new_basket.basket_phone_input
import kotlinx.android.synthetic.main.activity_new_basket.basket_picture_view
import kotlinx.android.synthetic.main.activity_new_basket.basket_publish_button
import kotlinx.android.synthetic.main.activity_new_basket.basket_validity_spinner
import kotlinx.android.synthetic.main.activity_new_basket.progress_bar
import kotlinx.android.synthetic.main.activity_new_basket.toolbar
import java.io.FileOutputStream
import javax.inject.Inject

class NewBasketActivity : AuthRequiredBaseActivity(), NewBasketContract.View, Injectable {

    @Inject
    lateinit var presenter: NewBasketContract.Presenter

    companion object {
        private const val REQUEST_PICK_LOCATION = 3

        private const val STATE_COORDINATE = "coordinate"
    }

    private var coordinate: Coordinate? = null

    var mapSnapshotDetachAction: (() -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attach(this)
        rootLayoutID = R.id.new_basket_root
        setContentView(R.layout.activity_new_basket)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.basket_new_title)

        val pictureFragment = basket_picture_view as PictureFragment
        pictureFragment.setCanTakePhoto(true)

        // if an external picture is given, copy it to a new file
        if (intent?.action == Intent.ACTION_SEND && intent.type?.startsWith("image/") == true) {
            (intent.getParcelableExtra<Parcelable>(Intent.EXTRA_STREAM) as? Uri)?.let { uri ->
                contentResolver.openInputStream(uri)?.let { input ->
                    val tempFile = Utils.createImageFile(this)
                    val output = FileOutputStream(tempFile)
                    output.use {
                        input.copyTo(it)
                        it.flush()
                    }
                    pictureFragment.file = tempFile
                }
            }
        }

        basket_checkbox_phone.setOnCheckedChangeListener { _, checked ->
            basket_phone_input.visibility = if (checked) VISIBLE else GONE
            basket_mobile_input.visibility = if (checked) VISIBLE else GONE
        }
        basket_publish_button.setOnClickListener { publishBasket() }

        if (savedInstanceState == null) {
            presenter.fetch()
        } else if (savedInstanceState.containsKey(STATE_COORDINATE)) {
            updateBasketMap(savedInstanceState.getParcelable(STATE_COORDINATE))
        }

        if (savedInstanceState == null) {
            // Default to a lifetime of two days
            val defaultIndex = resources.getStringArray(R.array.basket_validity_values).indexOf("2")
            basket_validity_spinner.setSelection(defaultIndex)
        }
    }

    override fun showDefaults(profile: Profile?, contactByMessage: Boolean, contactByPhone: Boolean) {
        if (basket_phone_input.text.isNullOrBlank() && !profile?.landline.isNullOrBlank()) {
            basket_phone_input.setText(profile?.landline?.trim())
        }
        if (basket_mobile_input.text.isNullOrBlank() && !profile?.mobile.isNullOrBlank()) {
            basket_mobile_input.setText(profile?.mobile?.trim())
        }
        basket_checkbox_message.isChecked = contactByMessage
        basket_checkbox_phone.isChecked = contactByPhone

        updateBasketMap(profile?.getCoordinates())
    }

    private fun updateBasketMap(coordinate: Coordinate?) {
        this.coordinate = coordinate

        val pickerCoordinate = coordinate ?: Coordinate(51.0, 10.0)
        val zoom = if (coordinate != null) DETAIL_MAP_ZOOM else 4.0
        val markerBitmap = if (coordinate != null) {
            getBasketMarkerIconBitmap(this.applicationContext)
        } else {
            null
        }
        mapSnapshotDetachAction = OsmdroidUtils.loadMapTileToImageView(basket_location_view, pickerCoordinate, zoom, markerBitmap)

        basket_location_view.setOnClickListener {
            val intent = Intent(this, PickLocationActivity::class.java)
            intent.putExtra(PickLocationActivity.EXTRA_COORDINATE, pickerCoordinate)
            intent.putExtra(PickLocationActivity.EXTRA_MARKER_ID, R.string.marker_basket_id)
            intent.putExtra(PickLocationActivity.EXTRA_ZOOM, zoom)
            startActivityForResult(intent, REQUEST_PICK_LOCATION)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onDestroy() {
        mapSnapshotDetachAction?.invoke()

        presenter.unsubscribe()
        super.onDestroy()
    }

    override fun display(basket: Basket) {
        // show details of created basket in an activity
        val intent = Intent(this, BasketActivity::class.java).also { intent ->
            intent.putExtra(BasketActivity.EXTRA_BASKET_ID, basket.id)
        }
        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }

    override fun showError(message: String) {
        progress_bar.visibility = GONE
        showMessage(message)
        basket_publish_button.isEnabled = true
    }

    /**
     * Checks the input and publishes the basket.
     */
    private fun publishBasket() {
        basket_publish_button.isEnabled = false

        // collect all input values
        val description = basket_description_input.text.toString().trim()
        val contactByMessage = basket_checkbox_message.isChecked
        val contactByPhone = basket_checkbox_phone.isChecked
        val phone =
                if (basket_checkbox_phone.isChecked) basket_phone_input.text.toString().trim() else null
        val mobile =
                if (basket_checkbox_phone.isChecked) basket_mobile_input.text.toString().trim() else null
        val lifetimeIdx = basket_validity_spinner.selectedItemPosition
        val lifetime =
                resources.getStringArray(R.array.basket_validity_values)[lifetimeIdx].toInt()

        // simple client-side validation
        var error: String? = null
        if (description.isEmpty()) {
            error = getString(R.string.basket_error_description)
        } else if (!contactByMessage && !contactByPhone) {
            error = getString(R.string.basket_error_contacttype)
        } else if (contactByPhone && (phone.isNullOrEmpty() && mobile.isNullOrEmpty())) {
            error = getString(R.string.basket_error_phone)
        } else if (coordinate == null) {
            error = getString(R.string.basket_error_location)
        }

        if (error != null) {
            showMessage(error, Snackbar.LENGTH_LONG)
            basket_publish_button.isEnabled = true
        } else {
            progress_bar.visibility = VISIBLE
            presenter.publish(
                    description,
                    phone,
                    mobile,
                    contactByMessage,
                    lifetime,
                    coordinate,
                    (basket_picture_view as PictureFragment).file
            )
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        coordinate?.let { outState.putParcelable(STATE_COORDINATE, it) }

        super.onSaveInstanceState(outState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_PICK_LOCATION -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    updateBasketMap(
                            data.getParcelableExtra(PickLocationActivity.EXTRA_COORDINATE)
                    )
                }
            }
        }
    }
}
