package de.foodsharing.ui.picture

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.picture.PictureActivity.Companion.EXTRA_PICTURE_FILE
import de.foodsharing.utils.Utils
import de.foodsharing.utils.captureException
import kotlinx.android.synthetic.main.fragment_picture.add_picture_button
import kotlinx.android.synthetic.main.fragment_picture.picture_view
import kotlinx.android.synthetic.main.fragment_picture.view.add_picture_button
import kotlinx.android.synthetic.main.fragment_picture.view.picture_view
import java.io.File
import java.io.IOException

/**
 * A UI snippet that displays a picture and handles the possibility to take a photo. The embedding
 * activity should be an instance of [BaseActivity].
 */
class PictureFragment : Fragment() {

    companion object {
        private const val REQUEST_IMAGE_CAPTURE = 1
        private const val REQUEST_SHOW_IMAGE = 2

        private const val STATE_PICTURE_FILE = "picture_file"
        private const val STATE_REQUESTED_FILE = "requested_file"
    }

    // The current image
    var file: File? = null
        set(value) {
            field = value
            showFile()
        }
    var pictureUri: String? = null
        set(value) {
            field = value
            showPictureFromUri()
        }
    // A temporary file where the captured image is written to
    private var requestedFile: File? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_picture, container, false)

        view.add_picture_button.setOnClickListener { capturePhoto() }
        view.picture_view.setOnClickListener {
            file?.path?.let { path ->
                startActivityForResult(
                    Intent(context, PictureActivity::class.java).apply {
                        putExtra(EXTRA_PICTURE_FILE, path)
                        putExtra(PictureActivity.EXTRA_SHOW_DELETE_BUTTON, true)
                    }, REQUEST_SHOW_IMAGE
                )
                activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
            }
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        savedInstanceState?.let { state ->
            (state.getSerializable(STATE_PICTURE_FILE) as? File?)?.let {
                file = it
            }
            (state.getSerializable(STATE_REQUESTED_FILE) as? File?)?.let {
                requestedFile = it
            }
        }
    }

    override fun onDestroy() {
        if (activity?.isFinishing == true) {
            requestedFile?.delete()
            file?.delete()
        }

        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_SHOW_IMAGE -> {
                if (resultCode == Activity.RESULT_CANCELED) {
                    // remove picture from view
                    file = null
                    picture_view.setImageBitmap(
                        BitmapFactory.decodeResource(
                            resources,
                            R.drawable.basket_default_picture
                        )
                    )
                }
            }
            REQUEST_IMAGE_CAPTURE -> {
                if (resultCode == Activity.RESULT_OK) {
                    // show picture in view
                    file = requestedFile
                    requestedFile = null
                } else {
                    requestedFile = null
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        file?.let {
            outState.putSerializable(STATE_PICTURE_FILE, it)
        }
        requestedFile?.let {
            outState.putSerializable(STATE_REQUESTED_FILE, it)
        }

        super.onSaveInstanceState(outState)
    }

    fun setCanTakePhoto(canTake: Boolean) {
        if (canTake) add_picture_button.show()
        else add_picture_button.hide()
    }

    /**
     * Starts the camera activity to take a photo.
     */
    private fun capturePhoto() {
        try {
            context?.let { ctx ->
                Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { intent ->
                    intent.resolveActivity(ctx.packageManager)?.also {
                        requestedFile = Utils.createImageFile(ctx).also {
                            val photoURI: Uri =
                                if (Build.VERSION.SDK_INT >= 24) {
                                    FileProvider.getUriForFile(
                                        ctx, "${BuildConfig.APPLICATION_ID}.fileprovider", it
                                    )
                                } else {
                                    Uri.fromFile(it)
                                }
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        }

                        startActivityForResult(
                            intent,
                            REQUEST_IMAGE_CAPTURE
                        )
                    }
                }
            }
        } catch (e: IOException) {
            captureException(e)
        }
    }

    /**
     * Updates the currently shown image to [file], if it is not null.
     */
    private fun showFile() {
        file?.let { f ->
            Picasso.get()
                .load(f)
                .fit()
                .centerCrop()
                .error(R.drawable.basket_default_picture)
                .into(picture_view)
        }
    }

    private fun showPictureFromUri() {
        pictureUri?.let { f ->
            Picasso.get()
                .load(f)
                .fit()
                .centerCrop()
                .error(R.drawable.basket_default_picture)
                .into(picture_view)
        }
    }
}
