package de.foodsharing.ui.conversation

import android.content.Context
import com.stfalcon.chatkit.utils.DateFormatter
import de.foodsharing.R
import java.util.Date

/**
 * Formats the date that is displayed above chat messages.
 */
class MessageDateFormatter(val context: Context) : DateFormatter.Formatter {

    override fun format(date: Date): String {
        return when {
            DateFormatter.isToday(date) -> context.getString(R.string.date_today)
            DateFormatter.isYesterday(date) -> context.getString(R.string.date_yesterday)
            else -> DateFormatter.format(date, DateFormatter.Template.STRING_DAY_MONTH_YEAR)
        }
    }
}