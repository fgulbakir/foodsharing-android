package de.foodsharing.ui.conversation

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.stfalcon.chatkit.messages.FsMessageListAdapter
import com.stfalcon.chatkit.messages.MessagesListAdapter
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.ConversationDetail
import de.foodsharing.model.Message
import de.foodsharing.model.User
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.base.ErrorStateRetrySnackbar
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.profile.ProfileActivity
import de.foodsharing.ui.users.UserListActivity
import de.foodsharing.utils.ChatkitPicassoImageLoader
import kotlinx.android.synthetic.main.activity_conversation.container
import kotlinx.android.synthetic.main.activity_conversation.message_input
import kotlinx.android.synthetic.main.activity_conversation.progress_bar
import kotlinx.android.synthetic.main.activity_conversation.recycler_view
import kotlinx.android.synthetic.main.activity_conversation.toolbar
import java.util.Date
import javax.inject.Inject

class ConversationActivity : BaseActivity(), Injectable {

    companion object {
        const val EXTRA_CONVERSATION_ID = "cid"
        const val EXTRA_FOODSHARER_ID = "fsid"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: ConversationViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ConversationViewModel::class.java)
    }

    private var messageAdapter: FsMessageListAdapter? = null
    private var messageAdapterDiff: AsyncListDiffer<MessagesListAdapter<ChatkitMessage>.Wrapper<Any>>? = null

    private lateinit var menu: Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rootLayoutID = R.id.container
        setContentView(R.layout.activity_conversation)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        message_input.visibility = GONE

        message_input.setInputListener { message ->
            val trimmedMessage = message.toString().trim()
            if (trimmedMessage.isNotEmpty()) {
                viewModel.sendMessage(trimmedMessage)
            }

            // Always return false since we clear the message manually
            false
        }

        if (intent.hasExtra(EXTRA_CONVERSATION_ID)) {
            viewModel.id = intent.getIntExtra(EXTRA_CONVERSATION_ID, -1)
        } else if (intent.hasExtra(EXTRA_FOODSHARER_ID)) {
            viewModel.foodsharerId = intent.getIntExtra(EXTRA_FOODSHARER_ID, -1)
        } else {
            showMessage(getString(R.string.conversation_404))
        }

        bindViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menu = menu
        menuInflater.inflate(R.menu.conversation_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.action_copy -> {
            messageAdapter?.let {
                val copyMessage = resources.getQuantityString(
                    R.plurals.copied_messages,
                    it.selectedMessages.size,
                    it.selectedMessages.size
                )
                super.showMessage(copyMessage, duration = Snackbar.LENGTH_SHORT)
                it.copySelectedMessagesText(this, {
                    it.text
                }, true)
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun bindViewModel() {
        viewModel.currentUserId.observe(this, Observer {
            initAdapter(senderId = it.toString())

            // If messages not shown yet but loaded, display them
            if (message_input.visibility != VISIBLE) {
                viewModel.conversation.value?.let { display(it) }
            }
        })

        viewModel.updateInput.observe(this, EventObserver {
            message_input.inputEditText.setText(it)
        })

        viewModel.isSendingMessage.observe(this, Observer {
            message_input.button.isEnabled = !it
        })

        viewModel.conversation.observe(this, Observer {
            display(it)
        })

        viewModel.showError.observe(this, EventObserver {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })

        ErrorStateRetrySnackbar(lifecycle, viewModel.errorState, viewModel::tryAgain, this, container)
    }

    private fun initAdapter(senderId: String) {
        val adapter = FsMessageListAdapter(senderId,
            ChatkitPicassoImageLoader()
        )
        adapter.setDateHeadersFormatter(MessageDateFormatter(this))
        adapter.enableSelectionMode {
            menu.findItem(R.id.action_copy).isVisible = it > 0
        }

        adapter.registerViewClickListener(R.id.messageUserAvatar) { _, message ->
            val intent = Intent(this, ProfileActivity::class.java)
            intent.putExtra(ProfileActivity.EXTRA_USER_ID, message.message.fsId)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }

        adapter.setLoadMoreListener { _, _ -> viewModel.loadNext() }

        recycler_view.setAdapter(adapter)

        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)

                // If a new message was added, scroll to it
                if (positionStart == 0) {
                    recycler_view.layoutManager?.scrollToPosition(0)
                }
            }
        })

        messageAdapterDiff = AsyncListDiffer(adapter, object : DiffUtil.ItemCallback<MessagesListAdapter<ChatkitMessage>.Wrapper<Any>>() {
            override fun areItemsTheSame(oldItem: MessagesListAdapter<ChatkitMessage>.Wrapper<Any>, newItem: MessagesListAdapter<ChatkitMessage>.Wrapper<Any>): Boolean {
                if (oldItem.item is ChatkitMessage && newItem.item is ChatkitMessage) {
                    return (oldItem.item as ChatkitMessage).id == (newItem.item as ChatkitMessage).id
                }
                return oldItem.item == newItem.item
            }

            override fun areContentsTheSame(oldItem: MessagesListAdapter<ChatkitMessage>.Wrapper<Any>, newItem: MessagesListAdapter<ChatkitMessage>.Wrapper<Any>): Boolean {
                if (oldItem.item is ChatkitMessage && newItem.item is ChatkitMessage) {
                    return (oldItem.item as ChatkitMessage) == (newItem.item as ChatkitMessage)
                }
                if (oldItem.item is Date && newItem.item is Date) {
                    return (oldItem.item as Date) == (newItem.item as Date)
                }
                return false
            }
        })

        messageAdapter = adapter
    }

    private fun display(conversation: ConversationDetail) {
        messageAdapter?.let { adapter ->
            message_input.visibility = VISIBLE

            if (conversation.members.size > 2) {
                if (conversation.name != null) {
                    supportActionBar?.title = conversation.name
                    supportActionBar?.subtitle = formatUserNames(conversation.members)
                } else supportActionBar?.title = formatUserNames(conversation.members)
            } else {
                supportActionBar?.title = formatUserNames(conversation.members)
            }
            val sortedItems = sortByDate(conversation.messages).map {
                ChatkitMessage(it)
            }
            val sortedItemsWithHeaders = adapter.setItemsWithoutNotify(sortedItems)
            messageAdapterDiff?.submitList(sortedItemsWithHeaders.toList())

            progress_bar.visibility = GONE
            message_input.visibility = VISIBLE

            toolbar.setOnClickListener {
                val others = conversation.members.filter { it.id != viewModel.currentUserId.value }
                if (others.isEmpty()) showMessage(
                    getString(R.string.profile_404),
                    duration = Snackbar.LENGTH_SHORT
                )
                else openProfiles(others, conversation.name ?: getString(R.string.user_list_title))
            }
        }
    }

    private fun openProfiles(users: List<User>, title: String) {
        val intent = if (users.size == 1)
            Intent(this, ProfileActivity::class.java).apply {
                putExtra(ProfileActivity.EXTRA_USER, users[0])
            }
        else Intent(this, UserListActivity::class.java).apply {
            putExtra(UserListActivity.EXTRA_CONVERSATION_ID, viewModel.id)
            putExtra(UserListActivity.EXTRA_TITLE, title)
        }
        startActivity(intent)
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }

    private fun sortByDate(messages: List<Message>): List<Message> {
        return messages.sortedWith(compareByDescending { it.time.time })
    }

    /**
     * Formats the names of chat members for the title.
     */
    private fun formatUserNames(users: List<User>): String {
        return users.filter { it.id != viewModel.currentUserId.value }
            .mapNotNull { it.name }
            .joinToString(", ")
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }
}
