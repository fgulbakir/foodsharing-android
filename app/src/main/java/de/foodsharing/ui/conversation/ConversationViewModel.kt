package de.foodsharing.ui.conversation

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.model.ConversationDetail
import de.foodsharing.services.AuthService
import de.foodsharing.services.ConversationsService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import de.foodsharing.utils.captureException
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.subjects.PublishSubject
import java.io.IOException
import javax.inject.Inject

class ConversationViewModel @Inject constructor(
    private val conversationsService: ConversationsService,
    private val auth: AuthService
) : BaseViewModel() {
    var id: Int? = null
        set(value) {
            if (field != value) {
                field = value
                fetchConversation()
            }
        }
    var foodsharerId: Int? = null
        set(value) {
            if (field != value) {
                field = value
                fetchConversationId()
            }
        }

    val conversation = MutableLiveData<ConversationDetail>()
    val isLoading = MutableLiveData<Boolean>()
    val isSendingMessage = MutableLiveData<Boolean>().apply {
        value = false
    }
    val updateInput = MutableLiveData<Event<String>>()
    val currentUserId = MutableLiveData<Int>()
    val errorState = MutableLiveData<Int>()

    val showError = MutableLiveData<Event<Int>>()

    private val loadNextEvents = PublishSubject.create<Any>()
    private val tryAgainEvents = PublishSubject.create<Any>()

    init {
        isLoading.value = true

        request(auth.currentUser().retryWhen(getErrorHandler(isLoading)), {
            currentUserId.postValue(it.id)
        }, {
            // Should be handled by error handler
        })
    }

    private fun fetchConversationId() {
        foodsharerId?.let {
            request(conversationsService.user2conv(it).retryWhen(getErrorHandler(isLoading)), {
                id = it.data.cid
            }, {
                // Should be handled by error handler
            })
        }
    }

    private fun fetchConversation() {
        val conversationId = id
        if (conversationId == null) {
            conversation.value = null
            return
        }

        request(conversationsService.conversationPaged(conversationId, loadNextEvents.doOnNext {
            isLoading.postValue(true)
        }, getErrorHandler(isLoading)), {
            isLoading.postValue(false)
            conversation.postValue(it)
        }, {
            // Should be handled by error handler
        })
    }

    fun sendMessage(body: String) {
        val conversationId = id ?: return
        isSendingMessage.value = true
        request(conversationsService.send(conversationId, body), {
            updateInput.postValue(Event(""))
            isSendingMessage.postValue(false)
        }, {
            isSendingMessage.postValue(false)

            when (it) {
                is IOException -> {
                    // Network Error
                    showError.postValue(Event(R.string.error_no_connection))
                }
                else -> {
                    // HttpException or unknown exception
                    showError.postValue(Event(R.string.error_unknown))
                }
            }
        })
    }

    private fun getErrorHandler(loadingIndicator: MutableLiveData<Boolean>): ((Observable<Throwable>) -> ObservableSource<Any>) = {
        it.flatMap {
            when (it) {
                is IOException -> {
                    // Network Error
                    errorState.postValue(R.string.error_no_connection)
                }
                else -> {
                    // HttpException or unknown exception
                    // Should not happen
                    errorState.postValue(R.string.error_unknown)
                    captureException(it)
                }
            }
            loadingIndicator.postValue(false)
            tryAgainEvents.doOnNext {
                errorState.postValue(null)
                loadingIndicator.postValue(true)
            }
        }
    }

    fun loadNext() {
        loadNextEvents.onNext(true)
    }

    fun tryAgain() {
        tryAgainEvents.onNext(true)
    }
}