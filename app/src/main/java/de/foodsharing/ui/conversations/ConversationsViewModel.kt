package de.foodsharing.ui.conversations

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.api.UserResponse
import de.foodsharing.model.ConversationListEntry
import de.foodsharing.services.AuthService
import de.foodsharing.services.ConversationsService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.utils.captureException
import de.foodsharing.utils.combineWith
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.subjects.PublishSubject
import java.io.IOException
import javax.inject.Inject

class ConversationsViewModel @Inject constructor(
    private val conversationsService: ConversationsService,
    private val authService: AuthService
) : BaseViewModel() {
    private val loadNextEvents = PublishSubject.create<Any>()
    private val reloadEvents = PublishSubject.create<Any>()
    private val tryAgainEvents = PublishSubject.create<Any>()

    val conversations = MutableLiveData<List<ConversationListEntry>>()
    val currentUser = MutableLiveData<UserResponse>()

    val conversationsWithCurrentUser: LiveData<Pair<List<ConversationListEntry>?, UserResponse?>> =
        conversations.combineWith(currentUser) { list, userResponse -> Pair(list, userResponse) }

    private val isLoadingCurrentUser = MutableLiveData<Boolean>()
    private val isLoadingConversations = MutableLiveData<Boolean>()

    val isReloading = MutableLiveData<Boolean>()
    val errorState = MutableLiveData<Int>()

    val isLoading = isLoadingConversations
        .combineWith(isLoadingCurrentUser) { a, b -> a?.or(b ?: return@combineWith null) }
        .combineWith(errorState) { loading, errorState ->
            loading?.and(errorState == null) ?: false
        }

    init {
        initConversationLoading()

        isLoadingCurrentUser.postValue(true)
        request(authService.currentUser().retryWhen(getErrorHandler(isLoadingCurrentUser)), {
            isLoadingCurrentUser.postValue(false)
            currentUser.postValue(it)
        })
    }

    private fun initConversationLoading() {
        isLoadingConversations.value = true
        isReloading.value = false
        request(conversationsService.listPaged(reloadEvents.doOnNext {
            isReloading.postValue(true)
        }, loadNextEvents.doOnNext {
            isLoadingConversations.postValue(true)
        }, getErrorHandler(isLoadingConversations)), {
            conversations.postValue(it)

            isLoadingConversations.postValue(false)
            isReloading.postValue(false)

            errorState.postValue(null)
        }, {
            captureException(it)
        })
    }

    private fun getErrorHandler(loadingIndicator: MutableLiveData<Boolean>): ((Observable<Throwable>) -> ObservableSource<Any>) = {
        it.flatMap {
            when (it) {
                is IOException -> {
                    // Network Error
                    errorState.postValue(R.string.error_no_connection)
                }
                else -> {
                    // HttpException or unknown exception
                    // Should not happen
                    errorState.postValue(R.string.error_unknown)
                    captureException(it)
                }
            }
            loadingIndicator.postValue(false)
            isReloading.postValue(false)
            tryAgainEvents.doOnNext {
                errorState.postValue(null)
                loadingIndicator.postValue(true)
            }
        }
    }

    fun loadNext() {
        loadNextEvents.onNext(true)
    }

    fun refresh() {
        reloadEvents.onNext(true)
    }

    fun tryAgain() {
        tryAgainEvents.onNext(true)
    }
}
