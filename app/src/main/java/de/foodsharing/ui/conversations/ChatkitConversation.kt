package de.foodsharing.ui.conversations

import com.stfalcon.chatkit.commons.models.IDialog
import de.foodsharing.model.ConversationListEntry
import de.foodsharing.ui.conversation.ChatkitMessage
import de.foodsharing.ui.conversation.ChatkitUser
import de.foodsharing.utils.Utils
import kotlin.math.min
import kotlin.random.Random

data class ChatkitConversation(
    val conversation: ConversationListEntry,
    var message: ChatkitMessage,
    val currentUserID: Int
) : IDialog<ChatkitMessage> {
    var unread: Int = conversation.unread

    override fun getId() = conversation.id.toString()

    override fun getDialogPhoto(): String {
        // use up to 4 user pictures as the group picture, exclude current user
        val num = min(conversation.members.size - 1, 4)

        return if (num < 1) ""
        else conversation.members
                .filter { it.id != currentUserID }
                .shuffled(Random(System.currentTimeMillis() / 1000 / 3600 / 24))
                .subList(0, num)
                .joinToString("|") {
                    Utils.getUserPhotoURL(it, Utils.PhotoType.Q_130)
                }
    }

    override fun getDialogName(): String {
        // return the names of participants if conv. name == null, exclude user
        return conversation.name ?: conversation.members
                .filter { it.id != currentUserID }
                .shuffled(Random(System.currentTimeMillis() / 1000 / 3600 / 24))
                .mapNotNull { it.name }
                .joinToString("|")
    }

    override fun getUsers(): List<ChatkitUser> = conversation.members.map { ChatkitUser(it) }

    override fun getLastMessage(): ChatkitMessage = message

    override fun setLastMessage(message: ChatkitMessage) {
        this.message = message
    }

    override fun getUnreadCount() = unread
}