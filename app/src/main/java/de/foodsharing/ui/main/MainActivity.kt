package de.foodsharing.ui.main
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.baskets.BasketsFragment
import de.foodsharing.ui.login.LoginActivity
import de.foodsharing.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header.view.*
import javax.inject.Inject

class MainActivity : BaseActivity(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mainViewModel: MainViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
    }

    private val pagerAdapter = MainPagerAdapter(supportFragmentManager)
    private lateinit var drawerToggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
        }

        drawerToggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.drawer_open, R.string.drawer_close)
        drawerToggle.syncState()

        setupTabs()

        // set actions for menu items in the drawer
        nav_view.setNavigationItemSelectedListener { menuItem ->
            // close drawer when item is tapped
            drawer_layout.closeDrawers()

            when (menuItem.itemId) {
                R.id.nav_logout -> logout()
                R.id.nav_support -> Utils.openSupportEmail(this, R.string.support_email_subject_suffix)
                R.id.nav_shareApp -> shareApp()
            }

            true
        }

        version_text_view.text = getString(R.string.version, BuildConfig.VERSION_NAME)

        bindViewModel()
    }

    private fun shareApp() {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.setType("text/plain")
        shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_recommend_text))
        startActivity(Intent.createChooser(shareIntent, getString(R.string.share_dialog_title)))
    }

    private fun setupTabs() {
        main_pager.adapter = pagerAdapter
        main_tab_layout.setupWithViewPager(main_pager)

        val context = this
        val initiallySelectedTabIndex = 0
        val tabColor = R.color.colorPrimaryABitLighter
        val tabColorSelected = R.color.colorPrimaryLight

        main_tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val textView = tab?.customView as TextView
                textView.setTextColor(ContextCompat.getColor(context, tabColorSelected))
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                val textView = tab?.customView as TextView
                textView.setTextColor(ContextCompat.getColor(context, tabColor))
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })

        for (position in 0 until pagerAdapter.count) {
            main_tab_layout.getTabAt(position)?.apply {
                val textView = LayoutInflater.from(context).inflate(pagerAdapter.getPageTextView(position), null) as TextView
                if (position == initiallySelectedTabIndex) {
                    textView.setTextColor(ContextCompat.getColor(context, tabColorSelected))
                } else {
                    textView.setTextColor(ContextCompat.getColor(context, tabColor))
                }
                customView = textView
            }
        }
    }

    private fun bindViewModel() {
        mainViewModel.currentUserName.observe(this, Observer<String> {
            // Show the current user name in the side bar
            nav_view.getHeaderView(0).account_name.text = it
        })

        mainViewModel.popup.observe(this, EventObserver {
            Utils.handlePopup(this, it)
        })
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        drawerToggle.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Logs out the user and switches to the LoginActivity. This is called when the logout button
     * in the drawer is selected.
     */
    private fun logout() {
        mainViewModel.logout()

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            BasketsFragment.PERMISSION_CODE -> if (grantResults.firstOrNull() == PackageManager.PERMISSION_GRANTED) {

                pagerAdapter.getItem(1).apply {
                    if (this is BasketsFragment) {
                        this.resetUsersLocation()
                        this.hideLocationButton()
                    }
                }
            }
        }
        return
    }
}
