package de.foodsharing.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import de.foodsharing.R
import de.foodsharing.ui.baskets.BasketsFragment
import de.foodsharing.ui.conversations.ConversationsFragment
import de.foodsharing.ui.map.MapFragment

class MainPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val conversationsFragment = ConversationsFragment()
    private val basketsFragment = BasketsFragment()
    private val mapFragment = MapFragment()

    override fun getCount(): Int = 3

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> conversationsFragment
        1 -> basketsFragment
        2 -> mapFragment
        else -> throw Exception("invalid tab id!")
    }

    fun getPageTextView(position: Int): Int = when (position) {
        0 -> R.layout.tab_conversation
        1 -> R.layout.tab_basket
        2 -> R.layout.tab_map
        else -> throw Exception("invalid tab id!")
    }
}