package de.foodsharing.ui.profile

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.api.ProfileAPI
import de.foodsharing.model.User
import de.foodsharing.services.AuthService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import retrofit2.HttpException
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
    private val profiles: ProfileAPI,
    auth: AuthService
) : BaseViewModel() {
    var userId: Int? = null
        set(value) {
            if (field != value) {
                field = value
                isCurrentUser.value = currentUserId != null && field == currentUserId

                if (profile.value?.id != field) fetch()
            }
        }

    val isCurrentUser = MutableLiveData<Boolean>().apply { value = false }
    val isLoading = MutableLiveData<Boolean>()
    val showError = MutableLiveData<Event<Int>>()
    val profile = MutableLiveData<User>()

    private var currentUserId: Int? = null

    init {
        request(auth.currentUser(), {
            currentUserId = it.id
            isCurrentUser.value = userId != null && userId == currentUserId
        })
    }

    private fun fetch() {
        if (userId != null) {
            isLoading.value = true

            request(profiles.getUser(userId!!), { user ->
                isLoading.value = false
                profile.value = user
            }, {
                isLoading.value = false
                handleError(it)
            })
        } else {
            profile.value = null
        }
    }

    private fun handleError(error: Throwable) {
        val stringRes = if (error is HttpException && error.code() == 404) {
            R.string.profile_404
        } else {
            R.string.error_unknown
        }
        showError.value = Event(stringRes)
    }
}
