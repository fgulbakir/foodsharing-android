package de.foodsharing.ui.basket

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.conversation.ConversationActivity
import de.foodsharing.ui.editbasket.EditBasketActivity
import de.foodsharing.ui.picture.PictureActivity
import de.foodsharing.ui.picture.PictureActivity.Companion.EXTRA_PICTURE_URL
import de.foodsharing.ui.profile.ProfileActivity
import de.foodsharing.utils.BASE_URL
import de.foodsharing.utils.BASKET_MAP_ZOOM
import de.foodsharing.utils.LINK_BASE_URL
import de.foodsharing.utils.OsmdroidUtils
import de.foodsharing.utils.Utils
import de.foodsharing.utils.Utils.getBasketMarkerIconBitmap
import de.foodsharing.utils.getDisplayName
import kotlinx.android.synthetic.main.activity_basket.basket_content_view
import kotlinx.android.synthetic.main.activity_basket.basket_created_at
import kotlinx.android.synthetic.main.activity_basket.basket_creator_name
import kotlinx.android.synthetic.main.activity_basket.basket_creator_picture
import kotlinx.android.synthetic.main.activity_basket.basket_description
import kotlinx.android.synthetic.main.activity_basket.basket_location_view
import kotlinx.android.synthetic.main.activity_basket.basket_message_button
import kotlinx.android.synthetic.main.activity_basket.basket_picture
import kotlinx.android.synthetic.main.activity_basket.basket_request_button
import kotlinx.android.synthetic.main.activity_basket.basket_request_count_label
import kotlinx.android.synthetic.main.activity_basket.basket_request_label
import kotlinx.android.synthetic.main.activity_basket.basket_request_phone
import kotlinx.android.synthetic.main.activity_basket.basket_valid_until
import kotlinx.android.synthetic.main.activity_basket.basket_withdraw_request_button
import kotlinx.android.synthetic.main.activity_basket.progress_bar
import kotlinx.android.synthetic.main.activity_basket.separator_request_basket
import kotlinx.android.synthetic.main.activity_basket.distance_to_basket
import kotlinx.android.synthetic.main.activity_basket.toolbar
import kotlinx.android.synthetic.main.dialog_request_basket.basket_request_message_input
import java.text.SimpleDateFormat
import java.util.Locale
import javax.inject.Inject
import kotlin.math.roundToInt

class BasketActivity : BaseActivity(), Injectable {

    companion object {
        private const val BASKET_URL = "$LINK_BASE_URL/essenskoerbe/%d"

        const val EXTRA_BASKET_ID = "id"
    }

    private val dateFormat = SimpleDateFormat("EEE, d MMM yyyy HH:mm", Locale.getDefault())

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: BasketViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(BasketViewModel::class.java)
    }

    var mapSnapshotDetachAction: (() -> Unit)? = null
    private var menu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rootLayoutID = R.id.basket_content
        setContentView(R.layout.activity_basket)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        bindViewModel()

        if (intent.hasExtra(EXTRA_BASKET_ID)) {
            val id = intent.getIntExtra(EXTRA_BASKET_ID, -1)
            supportActionBar?.title = "${getString(R.string.basket_label_short)} #$id"
            viewModel.basketId = id
        } else if (intent.data?.pathSegments?.firstOrNull() == "essenskoerbe") {
            val id = intent.data!!.pathSegments.last().toIntOrNull()
            if (id != null) {
                supportActionBar?.title = "${getString(R.string.basket_label_short)} #$id"
                viewModel.basketId = id
            } else {
                showBasket404()
            }
        } else {
            showBasket404()
        }
    }

    private fun bindViewModel() {
        viewModel.isLoading.observe(this, Observer<Boolean> {
            progress_bar.visibility = if (it) VISIBLE else INVISIBLE
        })

        viewModel.showInfo.observe(this, EventObserver {
            Toast.makeText(this, getString(it), Toast.LENGTH_LONG).show()
        })

        viewModel.showError.observe(this, EventObserver {
            showErrorMessage(getString(it))
        })

        viewModel.basket.observe(this, Observer<Basket> {
            display(it)
        })

        viewModel.basketRemoved.observe(this, EventObserver {
            finish()
        })

        viewModel.isCurrentUser.observe(this, Observer<Boolean> {
            updateCurrentUser(it)
        })

        viewModel.distance.observe(this, Observer<Double> {
            distance_to_basket.text = if (it < 1000) {
                "${it.roundToInt()} m"
            } else {
                "%.1f km".format(it / 1000)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.basket_menu, menu)
        this.menu = menu
        updateCurrentUser(viewModel.isCurrentUser.value == true)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.basket_copy_button -> {
            getBasketUrl()?.let {
                Utils.copyToClipboard(this, it)
                Toast.makeText(this, getString(R.string.copied_url), Toast.LENGTH_SHORT).show()
            }
            true
        }
        R.id.basket_share_button -> {
            getBasketUrl()?.let {
                Utils.openShareDialog(this, it)
            }
            true
        }
        R.id.basket_remove_button -> {
            viewModel.basket.value?.let {
                Utils.showQuestionDialog(this, getString(R.string.basket_remove_question)) { result ->
                    if (result) {
                        progress_bar.visibility = VISIBLE
                        viewModel.removeBasket()
                    }
                }
            }
            true
        }
        R.id.basket_edit_button -> {
            viewModel.basket.value?.let {
                startActivity(
                    Intent(this, EditBasketActivity::class.java)
                        .putExtra(EditBasketActivity.EXTRA_BASKET, it)
                )
            }
            true
        }
        R.id.basket_open_website_button -> {
            getBasketUrl()?.let {
                if (!Utils.openUrlInBrowser(this, it))
                    showErrorMessage(getString(R.string.browser_not_found))
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun getBasketUrl(): String? {
        return viewModel.basket.value?.let { BASKET_URL.format(it.id) }
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onDestroy() {
        mapSnapshotDetachAction?.invoke()
        super.onDestroy()
    }

    private fun display(basket: Basket) {
        basket_created_at.text = dateFormat.format(basket.createdAt)
        basket_valid_until.text = dateFormat.format(basket.until)
        basket_description.text = basket.description
        basket_creator_name.text = basket.creator.getDisplayName(this)

        invalidateOptionsMenu()

        basket_content_view.visibility = VISIBLE
        progress_bar.visibility = GONE

        // load basket picture
        if (basket.picture.isNullOrEmpty()) basket_picture.visibility = GONE
        else basket.picture?.let {
            val pictureUrl = "$BASE_URL/images/basket/$it"
            Picasso.get()
                .load(pictureUrl)
                .centerCrop()
                .fit()
                .error(R.drawable.basket_default_picture)
                .into(basket_picture)
            basket_picture.setOnClickListener {
                val intent = Intent(this, PictureActivity::class.java).apply {
                    putExtra(EXTRA_PICTURE_URL, pictureUrl)
                }
                startActivity(intent)
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
            }
            basket_picture.visibility = VISIBLE
        }

        // load user picture
        Picasso.get()
            .load(Utils.getUserPhotoURL(basket.creator, Utils.PhotoType.MINI))
            .fit()
            .centerCrop()
            .placeholder(R.drawable.default_user_picture)
            .error(R.drawable.default_user_picture)
            .into(basket_creator_picture)

        val listener = View.OnClickListener {
            val intent = Intent(this, ProfileActivity::class.java)
            intent.putExtra(ProfileActivity.EXTRA_USER, basket.creator)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }
        basket_creator_picture.setOnClickListener(listener)
        basket_creator_name.setOnClickListener(listener)

        updateCurrentUser(viewModel.isCurrentUser.value == true)

        val basketTitle = getString(R.string.basket_title, basket.creator.getDisplayName(this))

        mapSnapshotDetachAction = OsmdroidUtils.loadMapTileToImageView(basket_location_view, basket.toCoordinate(), BASKET_MAP_ZOOM,
                getBasketMarkerIconBitmap(this.applicationContext))
        basket_location_view.setOnClickListener {
            Utils.openCoordinate(this, basket.toCoordinate(), basketTitle)
        }
    }

    private fun showErrorMessage(error: String) {
        progress_bar.visibility = GONE
        showMessage(error, duration = Snackbar.LENGTH_LONG)
    }

    private fun showBasket404() {
        showErrorMessage(getString(R.string.basket_404))
    }

    /**
     * Updates the visibility of the option menu items and contact labels depending on whether the
     * current user is the owner of the basket.
     */
    private fun updateCurrentUser(isCurrentUser: Boolean) {
        val basket = viewModel.basket.value
        menu?.findItem(R.id.basket_remove_button)?.isVisible = isCurrentUser && basket != null
        menu?.findItem(R.id.basket_edit_button)?.isVisible = isCurrentUser && basket != null

        if (!isCurrentUser && basket != null && basket.contactTypes.isNotEmpty()) {
            separator_request_basket.visibility = VISIBLE
            basket_request_label.visibility = VISIBLE

            if (basket.contactTypes.contains(Basket.CONTACT_TYPE_MESSAGE)) {

                // Handle requests
                basket_request_count_label.visibility = VISIBLE
                basket_request_count_label.text = resources.getQuantityString(R.plurals.basket_request_number, basket.requestCount, basket.requestCount)

                val currentUserHasRequested = basket.requests.isNotEmpty()
                if (currentUserHasRequested) {
                    basket_message_button.visibility = VISIBLE
                    basket_withdraw_request_button.visibility = VISIBLE

                    basket_request_button.visibility = GONE

                    basket_message_button.setOnClickListener {
                        val intent = Intent(this, ConversationActivity::class.java)
                        intent.putExtra(ConversationActivity.EXTRA_FOODSHARER_ID, basket.creator.id)
                        startActivity(intent)
                    }

                    basket_withdraw_request_button.setOnClickListener {
                        Utils.showQuestionDialog(this, getString(R.string.basket_withdraw_question)) { result ->
                            if (result) {
                                viewModel.withdrawRequest()
                            }
                        }
                    }
                } else {
                    basket_request_button.visibility = VISIBLE

                    basket_message_button.visibility = GONE
                    basket_withdraw_request_button.visibility = GONE

                    basket_request_button.setOnClickListener {
                        lateinit var dialog: AlertDialog
                        val builder = AlertDialog.Builder(this)
                        builder.setTitle(R.string.request_button)
                        builder.setView(R.layout.dialog_request_basket)
                        builder.setPositiveButton(R.string.basket_request_dialog_label) { _, _ ->
                            viewModel.requestBasket(dialog.basket_request_message_input.text.toString())
                        }
                        builder.setNegativeButton(android.R.string.cancel, null)
                        dialog = builder.show()
                    }
                }
            } else {
                basket_message_button.visibility = GONE
                basket_request_count_label.visibility = GONE
                basket_request_button.visibility = GONE
                basket_withdraw_request_button.visibility = GONE
            }

            if (basket.contactTypes.contains(Basket.CONTACT_TYPE_PHONE)) {
                basket_request_phone.visibility = VISIBLE
                val phoneText = mutableListOf<CharSequence>()
                if (basket.phone.isNotEmpty()) {
                    phoneText.add("${getString(R.string.phone)}: ${basket.phone}")
                }
                if (basket.mobile.isNotEmpty()) {
                    phoneText.add("${getString(R.string.mobile)}: ${basket.mobile}")
                }
                basket_request_phone.text = phoneText.joinToString("\n")
            } else {
                basket_request_phone.visibility = GONE
            }
        } else {
            separator_request_basket.visibility = GONE
            basket_request_label.visibility = GONE

            basket_message_button.visibility = GONE
            basket_request_count_label.visibility = GONE
            basket_request_button.visibility = GONE
            basket_withdraw_request_button.visibility = GONE

            basket_request_phone.visibility = GONE
        }
    }
}
