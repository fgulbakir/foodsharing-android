package de.foodsharing.ui.basket

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import de.foodsharing.R
import de.foodsharing.model.Coordinate
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.map.BaseMapFragment
import de.foodsharing.utils.DEFAULT_MAP_ZOOM
import de.foodsharing.utils.Utils.getBasketMarkerIconBitmap
import kotlinx.android.synthetic.main.activity_pick_location.map_fragment
import kotlinx.android.synthetic.main.activity_pick_location.marker
import kotlinx.android.synthetic.main.activity_pick_location.toolbar

class PickLocationActivity : BaseActivity() {
    companion object {
        const val EXTRA_TITLE = "title"
        const val EXTRA_COORDINATE = "coordinate"
        const val EXTRA_ZOOM = "zoom"
        const val EXTRA_MARKER_ID = "marker_res_id"
    }

    private lateinit var baseMap: BaseMapFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pick_location)
        setSupportActionBar(toolbar)

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            if (intent.hasExtra(EXTRA_TITLE)) {
                title = intent.getStringExtra(EXTRA_TITLE)
            } else {
                setTitle(R.string.pick_location)
            }
        }

        baseMap = map_fragment as BaseMapFragment

        if (savedInstanceState == null) {
            if (intent.hasExtra(EXTRA_COORDINATE)) {
                val center = intent.getParcelableExtra<Coordinate>(EXTRA_COORDINATE)
                val zoom = intent.getDoubleExtra(EXTRA_ZOOM, DEFAULT_MAP_ZOOM)
                baseMap.updateLocation(center, zoom, animate = false)
            }
        }

        val markerId = intent.getIntExtra(EXTRA_MARKER_ID, R.drawable.marker_default)
        if (markerId == R.string.marker_basket_id)
            marker.setImageBitmap(getBasketMarkerIconBitmap(applicationContext))
        else
            marker.setImageResource(intent.getIntExtra(EXTRA_MARKER_ID, R.drawable.marker_default))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.ok_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                setResult(Activity.RESULT_CANCELED)
                finish()
                return true
            }
            R.id.action_ok -> {
                val data = Intent()
                data.putExtra(EXTRA_COORDINATE, baseMap.getMapCenter().toCoordinate())
                setResult(Activity.RESULT_OK, data)
                finish()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}