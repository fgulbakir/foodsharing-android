package de.foodsharing.api

import de.foodsharing.model.Basket
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Retrofit API interface for food baskets.
 */
interface BasketAPI {

    class BasketListResponse {

        var baskets: List<Basket>? = null
    }

    class BasketResponse {

        var basket: Basket? = null
    }

    /**
     * Requests the coordinates for all food baskets to be used on a map.
     */
    @GET("/api/baskets&type=coordinates")
    fun coordinates(): Observable<BasketListResponse>

    /**
     * Requests a list of all food baskets of the current user.
     */
    @GET("/api/baskets?type=mine")
    fun list(): Observable<BasketListResponse>

    /**
     * Requests a list of all food baskets close the given location.
     */
    @GET("/api/baskets/nearby")
    fun listClose(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("distance") distance: Int
    ): Observable<BasketListResponse>

    /**
     * Requests details of a food basket.
     */
    @GET("/api/baskets/{id}")
    fun get(@Path("id") id: Int): Observable<BasketResponse>

    /**
     * Removes a food basket if it belongs to the current user.
     */
    @DELETE("/api/baskets/{id}")
    fun remove(@Path("id") id: Int): Observable<Unit>

    /**
     * Creates a new food basket.
     */
    @FormUrlEncoded
    @POST("/api/baskets")
    fun create(
        @Field("description") description: String,
        @Field("contactTypes[]") contactTypes: Array<Int>,
        @Field("tel") phone: String?,
        @Field("handy") mobile: String?,
        @Field("lifetime") lifetime: Int,
        @Field("lat") lat: Double?,
        @Field("lon") lon: Double?
    ): Observable<BasketResponse>

    @PUT("/api/baskets/{id}/picture")
    fun setPicture(
        @Path("id") id: Int,
        @Body picture: RequestBody
    ): Observable<BasketResponse>

    @DELETE("/api/baskets/{id}/picture")
    fun removePicture(@Path("id") id: Int): Observable<BasketResponse>

    /**
     * Updates the description of an existing basket.
     */
    @FormUrlEncoded
    @PUT("/api/baskets/{id}")
    fun update(
        @Path("id") id: Int,
        @Field("description") description: String,
        @Field("lat") lat: Double,
        @Field("lon") lon: Double
    ): Observable<BasketResponse>

    @FormUrlEncoded
    @POST("/api/baskets/{id}/request")
    fun request(
        @Path("id") id: Int,
        @Field("message") message: String
    ): Observable<BasketResponse>

    @POST("/api/baskets/{id}/withdraw")
    fun withdrawRequest(
        @Path("id") id: Int
    ): Observable<BasketResponse>
}