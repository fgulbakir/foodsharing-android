package de.foodsharing.api

import de.foodsharing.model.FairSharePoint
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface FairSharePointAPI {

    @GET("/api/fairSharePoints/{id}")
    fun get(@Path("id") id: Int): Observable<FairSharePoint>
}