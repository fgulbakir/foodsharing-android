package de.foodsharing.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.view.ViewTreeObserver
import android.widget.ImageView
import de.foodsharing.BuildConfig
import de.foodsharing.model.Coordinate
import io.reactivex.Completable
import java.io.File
import org.osmdroid.api.IGeoPoint
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.MapTileProviderBasic
import org.osmdroid.tileprovider.tilesource.ITileSource
import org.osmdroid.tileprovider.tilesource.XYTileSource
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.drawing.MapSnapshot
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.CopyrightOverlay
import org.osmdroid.views.Projection

object OsmdroidUtils {
    fun initialise(context: Context) {
        Configuration.getInstance().osmdroidBasePath = File(context.externalCacheDir, "osmdroid")
        Configuration.getInstance().userAgentValue = "Foodsharing Android/${BuildConfig.VERSION_CODE} (it@foodsharing.network)"
    }

    private fun getTileSource(): ITileSource {
        return XYTileSource(
                "Wikimedia", 0, MAX_MAP_ZOOM.toInt(), 256, ".png",
                arrayOf("https://maps.wikimedia.org/osm-intl/"),
                "Wikimedia Maps | Map data © OpenStreetMap contributors"
        )
    }

    fun setupMapView(mapView: MapView) {
        mapView.setTileSource(getTileSource())

        // Scales tiles up according to DPI, tiles become pixelated but readable
        mapView.isTilesScaledToDpi = true

        // add default controls
        mapView.zoomController.setVisibility(CustomZoomButtonsController.Visibility.SHOW_AND_FADEOUT)
        mapView.setMultiTouchControls(true)

        mapView.setZoomRounding(false)
        mapView.maxZoomLevel = MAX_MAP_ZOOM

        mapView.isVerticalMapRepetitionEnabled = false

        mapView.overlays.add(CopyrightOverlay(mapView.context))
    }

    fun loadMapTileToImageView(imageView: ImageView, coordinate: Coordinate, zoom: Double, marker: Bitmap?): () -> Unit {
        val tileProvider = MapTileProviderBasic(imageView.context)
        tileProvider.setOfflineFirst(true)
        tileProvider.tileSource = getTileSource()
        tileProvider.createTileCache()

        var mapSnapshot: MapSnapshot? = null

        val run = {
            mapSnapshot = MapSnapshot(MapSnapshot.MapSnapshotable {
                if (it.status != MapSnapshot.Status.CANVAS_OK) {
                    return@MapSnapshotable
                }

                val map = it.bitmap

                if (marker != null) {
                    val canvas = Canvas(it.bitmap)
                    val paint = Paint()
                    canvas.drawBitmap(
                            marker,
                            map.width / 2.0f - marker.width / 2.0f,
                            map.height / 2.0f - marker.height,
                            paint
                    )
                }

                imageView.setImageBitmap(it.bitmap)
                it.onDetach()
                tileProvider.detach()
            }, MapSnapshot.INCLUDE_FLAG_UPTODATE, tileProvider, emptyList(),
                    Projection(zoom, imageView.width, imageView.height, coordinate.toGeoPoint(), 0f, true, true)
            )
            Completable.fromRunnable(mapSnapshot).subscribe()
        }

        // The height or width of the image view might still be zero at this point. If this is the
        // case wait for the layout to complete before initializing the snapshot.
        if (imageView.width > 0 && imageView.height > 0) {
            run()
        } else {
            imageView.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    if (imageView.width > 0 && imageView.height > 0) {
                        imageView.viewTreeObserver.removeOnPreDrawListener(this)
                        run()
                    }
                    return false
                }
            })
        }

        return {
            if (mapSnapshot?.status != MapSnapshot.Status.CANVAS_OK) {
                mapSnapshot?.onDetach()
            }
        }
    }

    fun IGeoPoint.toCoordinate(): Coordinate {
        return Coordinate(latitude, longitude)
    }

    fun Coordinate.toGeoPoint(): GeoPoint {
        return GeoPoint(lat, lon)
    }
}
