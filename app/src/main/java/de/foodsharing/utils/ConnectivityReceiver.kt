package de.foodsharing.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

/**
 * Utility class that notifies a listener about changes in the network connection.
 */
class ConnectivityReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val isConnected = isConnectedOrConnecting(context)
        connectivityReceiverListener?.onNetworkConnectionChanged(isConnected)
        connectivitySubject.onNext(isConnected)
    }

    private fun isConnectedOrConnecting(context: Context): Boolean {
        val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnectedOrConnecting
    }

    /**
     * Listener that is notified when the network connection changes.
     */
    interface ConnectivityReceiverListener {
        fun onNetworkConnectionChanged(isConnected: Boolean)
    }

    companion object {
        var connectivityReceiverListener: ConnectivityReceiverListener? = null
        private val connectivitySubject = BehaviorSubject.create<Boolean>()

        fun observe(): Observable<Boolean> {
            return connectivitySubject.hide()
        }
    }
}