package de.foodsharing.utils

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.stfalcon.chatkit.commons.ImageLoader
import de.foodsharing.R
import org.apache.commons.lang3.tuple.MutablePair
import kotlin.math.abs
import kotlin.math.max

/**
 * Implementation of the Chatkit [ImageLoader] for foodsharing using [Picasso].
 * Images can either be specified using a single url or using up to fours urls separated by a `|`.
 * In the latter case it loads all images in a single bitmap with dimensions
 * [R.dimen.dialog_avatar_width] and [R.dimen.dialog_avatar_height].
 *
 * In case of errors or if an empty url is provided, [R.drawable.default_user_picture] is loaded
 * into the [ImageView].
 */
class ChatkitPicassoImageLoader : ImageLoader {
    private val imageMargin = 4

    override fun loadImage(imageView: ImageView?, url: String?, payload: Any?) {
        if (imageView == null) {
            return
        }

        imageView.setImageBitmap(null)

        val urls = url?.split("|")
        if (urls != null && urls.size > 1) {
            if (urls.size > 4) {
                throw IllegalStateException()
            }

            val bitmapHolder: MutablePair<Bitmap?, Int> = MutablePair(null, urls.size)
            val r = imageView.context.resources
            val imageViewWidth = max(r.getDimension(R.dimen.dialog_avatar_width), r.getDimension(R.dimen.dialog_avatar_height)).toInt()

            val targets = mutableListOf<Target>()
            for ((i, u) in urls.withIndex()) {
                val target = ChatkitGroupTarget(
                    imageView,
                    i,
                    bitmapHolder,
                    (imageViewWidth - imageMargin) / 2,
                    imageMargin)
                targets += target

                Picasso.get()
                    .load(u)
                    .error(R.drawable.default_user_picture)
                    .into(target)
            }
            imageView.tag = targets
        } else if (url != null && url.isNotEmpty()) {
            Picasso.get()
                .load(url)
                .fit()
                .error(R.drawable.default_user_picture)
                .into(imageView)
        } else {
            Picasso.get()
                .load(R.drawable.default_user_picture)
                .into(imageView)
        }
    }

    /**
     * Special [Picasso] [Target] for foodsharing group conversation avatars. Each target represents
     * one of the up to four individual avatars combined into a single avatar.
     *
     * @property imageView The [ImageView] to load the final avatar into
     * @property index of the current avatar
     * @property bitmapHolder holder of the final bitmap and counter of currently loaded targets,
     * shared between all targets
     * @property targetSize width and height of the individual avatars
     * @property imageMargin margin between the avatars in the final image
     */
    class ChatkitGroupTarget(
        val imageView: ImageView,
        val index: Int,
        val bitmapHolder: MutablePair<Bitmap?, Int>,
        val targetSize: Int,
        val imageMargin: Int
    ) : Target {
        private val imageCount = bitmapHolder.right

        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
        }

        override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
            synchronized(bitmapHolder) {
                if (bitmapHolder.left == null) {
                    val result = Bitmap.createBitmap(
                        targetSize * 2 + imageMargin,
                        targetSize * 2 + imageMargin,
                        Bitmap.Config.ARGB_8888
                    )
                    bitmapHolder.left = result
                    bitmapHolder.right = imageCount
                }

                if (errorDrawable == null) {
                    bitmapHolder.right -= 1
                    return
                }

                val holderBitmap = bitmapHolder.left

                if (holderBitmap == null || bitmapHolder.right < 0) {
                    return
                }

                val canvas = Canvas(holderBitmap)
                val left = (targetSize + imageMargin) * (index % 2)
                val top = (targetSize + imageMargin) * (index / 2)
                val dest = Rect(left, top, left + targetSize - 1, top + targetSize - 1)
                errorDrawable.bounds = dest
                errorDrawable.draw(canvas)

                bitmapHolder.right -= 1
                imageView.setImageBitmap(holderBitmap)

                if (bitmapHolder.right == 0) {
                    bitmapHolder.left = null
                }
            }
        }

        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            synchronized(bitmapHolder) {
                if (bitmapHolder.left == null) {
                    val result = Bitmap.createBitmap(
                        targetSize * 2 + imageMargin,
                        targetSize * 2 + imageMargin,
                        Bitmap.Config.ARGB_8888
                    )
                    bitmapHolder.left = result
                    bitmapHolder.right = imageCount
                }

                if (bitmap != null) {
                    draw(bitmap)
                } else {
                    bitmapHolder.right -= 1
                }
            }
        }

        private fun draw(bitmap: Bitmap) {
            val holderBitmap = bitmapHolder.left ?: return

            val source = Rect(0, 0, bitmap.width - 1, bitmap.height - 1)
            val left = (targetSize + imageMargin) * (index % 2)
            val top = (targetSize + imageMargin) * (index / 2)
            val offset = (abs(bitmap.width - bitmap.height) / 2.0).toInt()
            val dest = if (bitmap.width < bitmap.height) {
                Rect(left + offset, top, left + targetSize - 1 - offset, top + targetSize - 1)
            } else {
                Rect(left, top + offset, left + targetSize - 1, top + targetSize - 1 - offset)
            }
            Canvas(holderBitmap).drawBitmap(bitmap, source, dest, Paint())

            bitmapHolder.right -= 1
            imageView.setImageBitmap(bitmapHolder.left)

            if (bitmapHolder.right == 0) {
                bitmapHolder.left = null
            }
        }
    }
}