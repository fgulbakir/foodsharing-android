package de.foodsharing.utils

import android.content.ClipData
import android.content.ClipboardManager
import android.content.ComponentName
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.Intent.EXTRA_SUBJECT
import android.content.Intent.EXTRA_TEXT
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Parcelable
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.exifinterface.media.ExifInterface
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.model.Coordinate
import de.foodsharing.model.Popup
import de.foodsharing.model.User
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.concurrent.TimeUnit

object Utils {

    /**
     * Creates a [Snackbar] via [Snackbar.make] and sets its background color.
     *
     * @param view The view to find a parent from.
     * @param text The text to show. Can be formatted text.
     * @param backgroundColor The background color.
     * @param duration How long to display the message. Either [Snackbar.LENGTH_SHORT] or
     * [Snackbar.LENGTH_LONG].
     *
     * @return the created Snackbar
     */
    fun showSnackBar(view: View, text: String, backgroundColor: Int, duration: Int): Snackbar {
        return Snackbar.make(
            view,
            text,
            duration
        ).withColor(backgroundColor)
    }

    /**
     * Shows a yes-no-dialog with a specific message and calls the result function afterwards.
     */
    fun showQuestionDialog(context: Context, message: String, result: (Boolean) -> Unit) {
        val listener = DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
                result(which == DialogInterface.BUTTON_POSITIVE)
        }

        AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, listener)
                .setNegativeButton(android.R.string.no, listener)
                .show()
    }

    enum class PhotoType(val prefix: String) {
        NORMAL(""), CROP("crop_"), THUMB_CROP("thumb_crop_"),
        Q_130("130_q_"), MINI("mini_q_")
    }

    fun getUserPhotoURL(user: User, type: PhotoType = Utils.PhotoType.Q_130): String {
        val filename = user.photo ?: user.avatar
        return if (filename != null && filename.isNotEmpty()) {
            "$BASE_URL/images/${type.prefix}$filename"
        } else DEFAULT_USER_PICTURE
    }

    enum class FairTeilerPhotoType(val prefix: String) {
        NORMAL(""), HEAD("crop_0_528_"), THUMB("crop_1_60_")
    }

    fun getFairteilerPhotoURL(
        picture: String,
        type: FairTeilerPhotoType = Utils.FairTeilerPhotoType.NORMAL
    ) =
        "$BASE_URL/images/${picture.replace("/", "/" + type.prefix)}"

    @Throws(IOException::class)
    fun createImageFile(context: Context): File {
        val directory = File(context.externalCacheDir, ANDROID_CACHE_SUBDIR)
        if (!directory.exists()) directory.mkdirs()
        clearCache(directory)

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        return File.createTempFile(
                "${PICTURE_FORMAT.toUpperCase()}_${timeStamp}_",
                ".$PICTURE_FORMAT", directory
        )
    }

    /**
     * Deletes all files in the cache directory that are older than [MAX_CACHE_DURATION_DAYS] days.
     */
    private fun clearCache(directory: File) {
        directory.listFiles()?.let {
            for (file in it) {
                if (TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis() - file.lastModified())
                    > MAX_CACHE_DURATION_DAYS
                )
                    file.delete()
            }
        }
    }

    /**
     * Loads an image from a file, scales and flips it according to the exif data, scales the image
     * down to maxSize and finally returns the image as a jpeg compressed ByteArray
     */
    fun prepareImageFileForUpload(file: File, maxSize: Int = 800): ByteArray {
        val input = file.inputStream()
        val exif = ExifInterface(input)
        val isFlipped = exif.isFlipped
        val rotation = exif.rotationDegrees

        val bitmap = BitmapFactory.decodeStream(file.inputStream())
        val matrix = Matrix()
        if (isFlipped) {
            matrix.postScale(-1.0f, 1.0f)
        }
        matrix.postRotate(rotation.toFloat())
        val ratio = Math.min(maxSize.toFloat() / bitmap.width, maxSize.toFloat() / bitmap.height)
        if (ratio < 1.0f) {
            // Scale down the image if it is larger the maxSize
            matrix.postScale(ratio, ratio)
        }

        val newBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        val stream = ByteArrayOutputStream()
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream)
        return stream.toByteArray()
    }

    fun openUrl(context: Context, url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        context.startActivity(intent)
    }

    /**
     * Opens the given URL in a browser, if one is installed, and excludes this app from the intent
     * list. Returns whether a browser could be found.
     */
    fun openUrlInBrowser(context: Context, url: String): Boolean {
        val intent = Intent(Intent.ACTION_VIEW)
        val uri = Uri.parse(url)
        intent.data = uri

        val shareIntentsLists = mutableListOf<Intent>()
        context.packageManager.queryIntentActivities(intent, 0).filter {
            "foodsharing" !in it.activityInfo.packageName.toLowerCase()
        }.forEach {
            shareIntentsLists += Intent().apply {
                component = ComponentName(
                    it.activityInfo.packageName,
                    it.activityInfo.name
                )
                action = Intent.ACTION_VIEW
                data = uri
            }
        }

        if (shareIntentsLists.isEmpty()) {
            return false
        }

        val chooserIntent = Intent.createChooser(
            shareIntentsLists.removeAt(0),
            context.getString(R.string.browser_chooser_title)
        ).apply {
            putExtra(Intent.EXTRA_INITIAL_INTENTS, shareIntentsLists.toTypedArray<Parcelable>())
        }

        context.startActivity(chooserIntent)
        return true
    }

    fun openSupportEmail(context: Context, @StringRes subject: Int) {
        val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", context.getString(R.string.support_email), null))
        intent.putExtra(EXTRA_SUBJECT, context.getString(R.string.support_email_subject, BuildConfig.VERSION_NAME, context.getString(subject)))
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.send_email_title)))
    }

    fun openShareDialog(context: Context, text: String, @StringRes titleId: Int = R.string.share_dialog_title) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(EXTRA_TEXT, text)
        context.startActivity(Intent.createChooser(intent, context.getString(titleId)))
    }

    fun copyToClipboard(context: Context, text: String) {
        val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText(null, text)
        clipboard.primaryClip = clip
    }

    fun openCoordinate(context: Context, coordinate: Coordinate, label: String = "") {
        // See https://developer.android.com/guide/components/intents-common#Maps
        val uri = Uri.parse("geo:0,0?q=${coordinate.lat},${coordinate.lon}($label)")
        val intent = Intent(Intent.ACTION_VIEW, uri)
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.open_location_title)))
    }

    /**
     * Compares a given [integer] to a [pattern]. The [pattern] is number optionally prefixed with
     * the following comparision operators: <, >, <=, >= and =.
     */
    fun compareIntToPattern(integer: Int, pattern: String): Boolean {
        val match = Regex("([><])?(=?)(\\d+)").matchEntire(pattern)
        return if (match != null) {
            val patternNumber = match.groupValues[3].toInt()
            if (match.groupValues[2].isNotEmpty() && patternNumber == integer) {
                true
            } else if (match.groupValues[1] == "<") {
                integer < patternNumber
            } else if (match.groupValues[1] == ">") {
                integer > patternNumber
            } else if (match.groupValues[1].isEmpty() && match.groupValues[2].isEmpty()) {
                patternNumber == integer
            } else {
                false
            }
        } else {
            false
        }
    }

    fun handlePopup(context: Context, popups: List<Popup>) {
        val popup = popups.find { it.isSatisfied(context.resources) }

        if (popup != null) {
            var dialog = AlertDialog.Builder(context)
                .setMessage(popup.body)
                .setTitle(popup.title)

            if (popup.positiveAction != null) {
                dialog = dialog.setPositiveButton(popup.positiveAction.title) { _, _ ->
                    popup.positiveAction.url?.let {
                        openUrl(context, it)
                    }
                }
            }

            if (popup.negativeAction != null) {
                dialog = dialog.setNegativeButton(popup.negativeAction.title, null)
            }

            dialog.show()
        }
    }

    private fun getBitmapFromDrawable(drawable: Drawable): Bitmap {
        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    fun getBitmapFromResourceId(context: Context, resourceId: Int): Bitmap? {
        val drawable = ContextCompat.getDrawable(context, resourceId)?.let {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                (DrawableCompat.wrap(it)).mutate()
            } else {
                it
            }
        } ?: return null

        return getBitmapFromDrawable(drawable)
    }

    private fun getBitmapFromText(context: Context, fontRes: Int, colorRes: Int, faIconRes: Int, textSize: Float): Bitmap {
        val fontAwesomeType = ResourcesCompat.getFont(context, fontRes)
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.textSize = textSize
        paint.color = ContextCompat.getColor(context, colorRes)
        paint.textAlign = Paint.Align.LEFT
        paint.typeface = fontAwesomeType

        val baseline = -paint.ascent()
        val width = (paint.measureText(context.resources.getString(faIconRes)))
        val height = (baseline + paint.descent())
        val bitmap = Bitmap.createBitmap(width.toInt(), height.toInt(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        canvas.drawText(context.resources.getString(faIconRes), 0F, baseline, paint)

        return bitmap
    }

    private fun getMarkerIcon(context: Context, colorRes: Int, faIconRes: Int): BitmapDrawable {
        val markerBitmap = getBitmapFromText(context, R.font.fa_solid_900, colorRes, R.string.fa_map_marker_solid, 100F)
        val iconBitmap = getBitmapFromText(context, R.font.fa_solid_900, android.R.color.white, faIconRes, 35F)

        val canvas = Canvas(markerBitmap)
        val iconPadding = (markerBitmap.width - iconBitmap.width) / 2F
        canvas.drawBitmap(iconBitmap, iconPadding, iconPadding, null)

        return BitmapDrawable(context.resources, markerBitmap)
    }

    fun getBasketMarkerIcon(context: Context) = getMarkerIcon(context, R.color.basketMarkerColor, R.string.fa_shopping_basket)

    fun getFspMarkerIcon(context: Context) = getMarkerIcon(context, R.color.fspMarkerColor, R.string.fa_recycle_solid)

    fun getBasketMarkerIconBitmap(context: Context) = getBitmapFromDrawable(getBasketMarkerIcon(context))

    fun getFspMakerIconBitmap(context: Context) = getBitmapFromDrawable(getFspMarkerIcon(context))
}
