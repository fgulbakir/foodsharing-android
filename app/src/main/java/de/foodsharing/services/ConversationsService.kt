package de.foodsharing.services

import android.util.Log
import de.foodsharing.api.ConversationsAPI
import de.foodsharing.api.WebsocketAPI
import de.foodsharing.model.ConversationDetail
import de.foodsharing.model.ConversationIdResponse
import de.foodsharing.model.ConversationListEntry
import de.foodsharing.model.Message
import de.foodsharing.model.User
import de.foodsharing.utils.CONVERSATIONS_PER_REQUEST
import de.foodsharing.utils.LOG_TAG
import de.foodsharing.utils.MESSAGES_PER_REQUEST
import de.foodsharing.utils.testing.OpenForTesting
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class ConversationsService @Inject constructor(
    private val conversationsApi: ConversationsAPI,
    private val ws: WebsocketAPI,
    private val auth: AuthService
) {
    private val readEvents = PublishSubject.create<Int>()

    private fun listPage(page: Int): Observable<List<ConversationListEntry>> {
        val overlap = if (page > 0) 1 else 0
        return conversationsApi.list(CONVERSATIONS_PER_REQUEST + overlap, page * CONVERSATIONS_PER_REQUEST - overlap)
    }

    /**
     * Returns an [Observable] of the list of conversations. The list gets updated in case new
     * messages are received via the [WebsocketAPI], a conversations is marked as read or in case
     * the list is reloaded or the next page of messages is loaded.
     * To trigger the latter the provided streams [reloadEvents] and [loadNextEvents] are observed.
     * In case of errors, the [errorHandler] is invoked which controls the retry behavior of the
     * stream.
     *
     * @param reloadEvents Observable triggering the reloading of the current list of conversations
     * @param loadNextEvents Observable triggering the load of the next page of conversations
     * @param errorHandler maps a stream of errors to retry events or cancels the stream
     */
    fun listPaged(reloadEvents: Observable<Any>, loadNextEvents: Observable<Any>, errorHandler: ((Observable<Throwable>) -> ObservableSource<Any>)): Observable<List<ConversationListEntry>> {
        // Observable of the individual chunks of conversations
        val pagedConversations = Observable.range(1, Integer.MAX_VALUE)
            .concatMap { page ->
                loadNextEvents.take(1)
                    .flatMap {
                        listPage(page).subscribeOn(Schedulers.io()).retryWhen(errorHandler)
                    }
            }.takeUntil {
                it.size < CONVERSATIONS_PER_REQUEST + 1
            }.subscribeOn(Schedulers.io())

        // An observables of updates to the current list of conversations
        // Can either be a new page, a new message or a conversation which was marked as read
        val updates = Observable.merge(
            pagedConversations.map {
                ConversationListUpdate.NewPageUpdate(it)
            },
            ws.subscribe().retryWhen(errorHandler).flatMap { message ->
                (message as? WebsocketAPI.ConversationMessage)?.let {
                    Observable.just(ConversationListUpdate.NewMessageUpdate(it))
                } ?: Observable.empty<ConversationListUpdate.NewMessageUpdate>()
            },
            readEvents.map { ConversationListUpdate.MarkedAsReadUpdate(it) }
        )

        val fullConversations = auth.currentUser()
            .subscribeOn(Schedulers.io())
            .retryWhen(errorHandler)
            .switchMap { currentUser ->

            // A subject which triggers to reload the whole conversation up to a specific index
            val loadNumSubject = PublishSubject.create<Int>()

            // A mapping which applies the updates to the current list of conversations or triggers
            // loadNumSubject if this is not possible to reload the conversations
            val conversationUpdater = { current: List<ConversationListEntry>, update: ConversationListUpdate ->
                when (update) {
                    is ConversationListUpdate.NewPageUpdate -> {
                        if (update.conversations.isEmpty()) {
                            current
                        } else if (current.last().id == update.conversations.first().id) {
                            current + update.conversations.subList(1, update.conversations.size)
                        } else {
                            loadNumSubject.onNext(current.size + update.conversations.size - 1)
                            current
                        }
                    }
                    is ConversationListUpdate.MarkedAsReadUpdate -> {
                        val newList = current.toMutableList()
                        val index = current.indexOfFirst {
                            it.id == update.id
                        }
                        if (index != -1) {
                            newList[index] = current[index].copy(unread = 0)
                        }
                        newList
                    }
                    is ConversationListUpdate.NewMessageUpdate -> {
                        val newList = current.toMutableList()
                        val index = current.indexOfFirst {
                            it.id == update.message.cid
                        }
                        if (index != -1) {
                            newList[index] = current[index].copy(
                                lastMessage = update.message.body,
                                lastFoodsaverID = update.message.fsId,
                                lastTS = update.message.time,
                                unread = if (update.message.fsId != currentUser.id) 1 else 0
                            )
                        } else {
                            // The message is a new one, reload the whole current list
                            loadNumSubject.onNext(current.size)
                        }
                        newList
                    }
                }.sortedWith(compareByDescending<ConversationListEntry> { it.unread }.thenByDescending { it.lastTS })
            }

            // Loads the initial conversations and applies subsequent updates
            loadNumSubject.startWith(CONVERSATIONS_PER_REQUEST).switchMap { numItems ->
                conversationsApi.list(numItems, 0)
                    .subscribeOn(Schedulers.io())
                    .retryWhen(errorHandler)
                    .switchMap { firstConversations ->
                        updates.scan(firstConversations, conversationUpdater)
                    }
            }
        }.subscribeOn(Schedulers.io())

        return Observable.just(Any()).concatWith(reloadEvents).switchMap {
            fullConversations
        }.doOnTerminate {
            Log.w(LOG_TAG, "TERMINATING CONVERSATIONS")
        }
    }

    private fun conversationPage(id: Int, page: Int): Observable<ConversationDetail> {
        val overlap = if (page > 0) 1 else 0
        return conversationsApi.get(id, MESSAGES_PER_REQUEST + overlap, page * MESSAGES_PER_REQUEST - overlap)
    }

    /**
     *
     * @param loadNextEvents Observable triggering the load of the next page of messages
     * @param errorHandler maps a stream of errors to retry events or cancels the stream
     */
    fun conversationPaged(conversationId: Int, loadNextEvents: Observable<Any>, errorHandler: ((Observable<Throwable>) -> ObservableSource<Any>)): Observable<ConversationDetail> {
        // Observable of the individual chunks of conversations
        val pagedConversations = Observable.range(1, Integer.MAX_VALUE)
            .concatMap { page ->
                loadNextEvents.take(1)
                    .flatMap {
                        conversationPage(conversationId, page).subscribeOn(Schedulers.io()).retryWhen(errorHandler)
                    }
            }.takeUntil {
                it.messages.size < MESSAGES_PER_REQUEST + 1
            }.subscribeOn(Schedulers.io())

        // An observables of updates to the current list of conversations
        // Can either be a new page, a new message or a conversation which was marked as read
        val updates = Observable.merge(
            pagedConversations.map {
                MessageListUpdate.NewPageUpdate(it)
            },
            ws.subscribe().retryWhen(errorHandler).flatMap {
                val message = it as? WebsocketAPI.ConversationMessage
                // Only consider updates for the current conversation
                if (message?.cid == conversationId) {
                    Observable.just(MessageListUpdate.NewMessageUpdate(it))
                } else {
                    Observable.empty<MessageListUpdate.NewMessageUpdate>()
                }
            }
        )

        val fullMessages = auth.currentUser()
            .subscribeOn(Schedulers.io())
            .retryWhen(errorHandler)
            .switchMap {

                // A subject which triggers to reload the whole conversation up to a specific index
                val loadNumSubject = PublishSubject.create<Int>()

                // A mapping which applies the updates to the current list of conversations or triggers
                // loadNumSubject if this is not possible to reload the conversations
                val conversationUpdater = { current: ConversationDetail, update: MessageListUpdate ->
                    when (update) {
                        is MessageListUpdate.NewPageUpdate -> {
                            if (update.conversations.messages.isEmpty()) {
                                current
                            } else if (current.messages.last().id == update.conversations.messages.first().id) {
                                val newMessages = current.messages + update.conversations.messages.subList(1, update.conversations.messages.size)
                                update.conversations.copy(messages = newMessages)
                            } else {
                                loadNumSubject.onNext(current.messages.size + update.conversations.messages.size - 1)
                                current
                            }
                        }
                        is MessageListUpdate.NewMessageUpdate -> {
                            val newMessageList = current.messages.toMutableList()
                            val newMessage = Message(
                                id = update.message.id,
                                fsId = update.message.fsId,
                                fsName = update.message.fsName,
                                body = update.message.body,
                                time = update.message.time,
                                fsPhoto = update.message.fsPhoto
                            )
                            newMessageList.add(0, newMessage)
                            current.copy(messages = newMessageList)
                        }
                    }
                }

                // Loads the initial conversations and applies subsequent updates
                loadNumSubject.startWith(MESSAGES_PER_REQUEST).switchMap { numItems ->
                    conversationsApi.get(conversationId, numItems, 0)
                        .subscribeOn(Schedulers.io())
                        .retryWhen(errorHandler)
                        .switchMap { firstConversations ->
                            updates.scan(firstConversations, conversationUpdater)
                        }
                }
            }.subscribeOn(Schedulers.io())

        return Observable.just(Any()).switchMap {
            fullMessages
        }.doOnTerminate {
            Log.w(LOG_TAG, "TERMINATING MESSAGES")
        }
    }

    fun user2conv(foodsharerId: Int): Observable<ConversationIdResponse> {
        return conversationsApi.user2conv(foodsharerId)
    }

    fun send(conversationId: Int, body: String): Observable<Boolean> {
        return conversationsApi.send(conversationId, body)
    }

    fun getUsers(conversationId: Int): Observable<List<User>> {
        return conversationsApi.get(conversationId, 0, 0).map { it.members }
    }

    sealed class ConversationListUpdate {
        data class NewPageUpdate(
            val conversations: List<ConversationListEntry>
        ) : ConversationListUpdate()

        data class MarkedAsReadUpdate(
            val id: Int
        ) : ConversationListUpdate()

        data class NewMessageUpdate(
            val message: WebsocketAPI.ConversationMessage
        ) : ConversationListUpdate()
    }

    sealed class MessageListUpdate {
        data class NewPageUpdate(
            val conversations: ConversationDetail
        ) : MessageListUpdate()

        data class NewMessageUpdate(
            val message: WebsocketAPI.ConversationMessage
        ) : MessageListUpdate()
    }
}