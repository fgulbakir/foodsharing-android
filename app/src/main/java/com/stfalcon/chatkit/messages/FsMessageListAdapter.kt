package com.stfalcon.chatkit.messages

import com.stfalcon.chatkit.commons.ImageLoader
import de.foodsharing.ui.conversation.ChatkitMessage

/**
 * Adapter for {@link MessagesList}.
 */
class FsMessageListAdapter(senderId: String, imageLoader: ImageLoader) : MessagesListAdapter<ChatkitMessage>(senderId, imageLoader) {
    /**
     * Updates the list of items without calling notify internally. Can be used in conjunction with
     * a custom ListDiffer.
     */
    fun setItemsWithoutNotify(items: List<ChatkitMessage>): List<Wrapper<Any>> {
        val oldSelected = this.items
                .filter { it.isSelected }
                .mapNotNull { (it.item as? ChatkitMessage)?.id }
        this.items.clear()
        generateDateHeaders(items)

        this.items
                .filter { (it.item as? ChatkitMessage)?.id in oldSelected }
                .forEach { it.isSelected = true }

        return this.items.map { Wrapper<Any>(it) }
    }
}
