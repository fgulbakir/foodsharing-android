# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## Unreleased

### Added
- Displays picture of baskets in list of baskets #108 !181 [@Srishti71](https://gitlab.com/Srishti71) [@linini](https://gitlab.com/linini)
- Allows to change picture of basket after basket already exists #25 !182 [@Srishti71](https://gitlab.com/Srishti71) [@linini](https://gitlab.com/linini)

- Allow to create and withdraw basket requests #71 !177 [@dthulke](https://gitlab.com/dthulke)

- New button for share/recommend the foodsharing app to other people #106 !185 [@TheSoulT](https://gitlab.com/thesoult)

### Changed

- Changed title of "pick basket place menu" #100 !186 [@TheSoulT](https://gitlab.com/thesoult)
- Refactored conversation view to MVVM #91 !175 [@dthulke](https://gitlab.com/dthulke)
- Refactored user list view to MVVM #91 !176 [@dthulke](https://gitlab.com/dthulke)
- Allows to add a picture of a basket from gallery #49 !180 [@Srishti71](https://gitlab.com/Srishti71) [@linini](https://gitlab.com/linini)
- Allows users to see distance to basket from current (phone's) location #76 #54 !179 [@Srishti71](https://gitlab.com/Srishti71) [@linini](https://gitlab.com/linini)
- Updated map markers to be consistent with web #78 !183 [@CeciliaHwang](https://gitlab.com/CeciliaHwang)

### Fixed

- Disabled zoom rounding in the map to avoid glitches !188 [@dthulke](https://gitlab.com/dthulke)

## [0.5.1] - 2019-09-22

### Added

### Changed

- Allows to open fair-share point and basket images in the picture activity and adds zoom and pan support to it #17 !172 [@dthulke](https://gitlab.com/dthulke)

### Fixed

- Fixed a bug which caused that the request section of a basket was not shown !173 [@alex.simm](https://gitlab.com/alex.simm)

## [0.5.0] - 2019-09-13

### Added

- Allow editing a basket's location #25 !146 [@alex.simm](https://gitlab.com/alex.simm)
- View for fair share points #30 !150 [@alex.simm](https://gitlab.com/alex.simm)
- Allow links to fair-share points to be opened in the app #63 !151 [@alex.simm](https://gitlab.com/alex.simm)
- Allow opening overlapping markers on the map #67 !163 [@dthulke](https://gitlab.com/dthulke)
- Shows the location of the user on the map #73 !164 [@dthulke](https://gitlab.com/dthulke)

### Changed

- Refactored profiles view to MVVM #91 !149 [@alex.simm](https://gitlab.com/alex.simm)
- Clarifies the labels of the basket lifetime picker #95 !161 [@dthulke](https://gitlab.com/dthulke)

### Fixed

- Fixed a bug which caused that long messages were not displayed correctly on some devices #68 !169 [@dthulke](https://gitlab.com/dthulke)

## [0.4.0] - 2019-08-14

### Added

- Link to the website for profiles and baskets #85 !125 [@alex.simm](https://gitlab.com/alex.simm)

### Changed

- Moved basket photo logic to PictureFragment class !127 [@alex.simm](https://gitlab.com/alex.simm)
- Reduced map zoom for baskets #80 !138 [@alex.simm](https://gitlab.com/alex.simm)

### Fixed

- Messages of jumpers are now shown in store conversation !130 [@dthulke](https://gitlab.com/dthulke)

## [0.3.1] - 2019-07-01

### Added

- User profiles with contact button #36 !104 [@alex.simm](https://gitlab.com/alex.simm)
- List of group chat members #47 !110 [@alex.simm](https://gitlab.com/alex.simm)

### Changed

### Fixed

## [0.3.0] - 2019-06-20

### Added

- Possibility to load more conversations #34 !59 [@alex.simm](https://gitlab.com/alex.simm)
- Map with the basket's location in the basket detail view !68 [@dthulke](https://gitlab.com/dthulke)
- Accept pictures from other apps to create a basket #49 !70 [@alex.simm](https://gitlab.com/alex.simm)
- A copy and a share url button to the basket detail view #39 !75 [@dthulke](https://gitlab.com/dthulke)
- Links to baskets can be opened via the app #63 !87 [@dthulke](https://gitlab.com/dthulke)
- Loading indicator for basket picture upload #61 !104 [@alex.simm](https://gitlab.com/alex.simm)

### Changed

- Improved the behaviour of the my location button #64 !88 [@dthulke](https://gitlab.com/dthulke)
- Possiblity to only show baskets or fair share points on the map #66 !89 [@dthulke](https://gitlab.com/dthulke)

### Fixed

- Fixed landscape mode for login screen #24 !57 [@alex.simm](https://gitlab.com/alex.simm)
- Fixes flickering and sometimes broken group profile images !63 [@dthulke](https://gitlab.com/dthulke)
- Reduced the size of the app #11 !65 [@dthulke](https://gitlab.com/dthulke)
- Basket picture in the NewBasketActivity is restored after configuration change !67 [@dthulke](https://gitlab.com/dthulke)
- Fixed a crash after aborting taking a picture while creating a basket #46 !73 [@dthulke](https://gitlab.com/dthulke)
- Styling of the buttons in dialogs !71 [@dthulke](https://gitlab.com/dthulke)
- Fixed line break problem in conversations #40 !79 [@alex.simm](https://gitlab.com/alex.simm)
- Use cache directory for temporary image file #60 !82 [@alex.simm](https://gitlab.com/alex.simm)
- Allow to scroll to the bottom of the new basket screen when the keyboard is open #74 [@dthulke](https://gitlab.com/dthulke)

## [0.2.0] - 2019-05-14

### Added

- Possibility to request baskets !44 [@dthulke](https://gitlab.com/dthulke)

### Fixed

- Using new logout endpoint #29 !52 [@alex.simm](https://gitlab.com/alex.simm)

## [0.1.0] - 2019-04-20

First public beta release!

### Fixed

- Downground okhttp to keep API 19 compataiblity
- Add null check to geo provider
- Fixed image picker crash
- Fixed tablayout styling
- Fixed support mail link

## [0.0.4] - 2019-04-11

### Added

- Add version name and extra links !40 [@dthulke](https://gitlab.com/dthulke)
- Map caching and cluster icons in Android 9 !36 [@dthulke](https://gitlab.com/dthulke)

### Fixed

- Fix unread message count #20 !38 [@dthulke](https://gitlab.com/dthulke)

### Removed

- Remove example credentials from login form #22 !39 [@dthulke](https://gitlab.com/dthulke)

## [0.0.3] - 2019-04-01

### Added

- Possibility to delete baskets #5 !31 [@alex.simm](https://gitlab.com/alex.simm).
- Zoom to current location on the map #12 !32 [@alex.simm](https://gitlab.com/alex.simm).

### Changed

- Loading all images via OkHttp and using caching #5 !31 [@alex.simm](https://gitlab.com/alex.simm).

## [0.0.2] - 2019-02-23

### Added

- nothing

## [0.0.1] - 2019-02-23

### Added

- everything
